package com.siapceria.rsudajibarang.siapceriaajb.adapter;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.siapceria.rsudajibarang.siapceriaajb.R;
import com.siapceria.rsudajibarang.siapceriaajb.fragment.Bantuan.Fragment_Bantuan_Detail;
import com.siapceria.rsudajibarang.siapceriaajb.model.mBantuan;

import java.util.ArrayList;

import es.dmoral.toasty.Toasty;

public class BantuanAdapter extends RecyclerView.Adapter<BantuanAdapter.MyViewHolder>{

    private LayoutInflater inflater;
    private ArrayList<mBantuan> dataModelArrayList;
    public static String KEY_ID = "id";
    public static String KEY_BANTUAN = "bantuan";
    public static String KEY_PENANGANAN = "penanganan";


    private Context context;
    public BantuanAdapter(Context context, ArrayList<mBantuan> dataModelArrayList){

        this.context=context;
        inflater = LayoutInflater.from(context);
        this.dataModelArrayList = dataModelArrayList;
//        this.listener = listener;
    }

    @Override
    public BantuanAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.listbantuan, parent, false);
        MyViewHolder holder = new MyViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(BantuanAdapter.MyViewHolder holder, int position) {

        holder.tvbantuanidlist.setText(dataModelArrayList.get(position).getId());
        holder.tvbenatuan.setText(dataModelArrayList.get(position).getBantuan());
        holder.tvbantuanpenangananlist.setText(dataModelArrayList.get(position).getPenanganan());
    }

    @Override
    public int getItemCount() {
        return dataModelArrayList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder  implements View.OnClickListener{

        TextView tvbenatuan,tvbantuanidlist,tvbantuanpenangananlist;

        public MyViewHolder(View itemView) {
            super(itemView);

            tvbenatuan = (TextView) itemView.findViewById(R.id.tvbantuanlist);
            tvbantuanidlist = (TextView) itemView.findViewById(R.id.tvbantuanidlist);
            tvbantuanpenangananlist = (TextView) itemView.findViewById(R.id.tvbantuanpenangananlist);

            itemView.setClickable(true);
            itemView.setOnClickListener(this);
        }

        public void onClick(View v) {
            Fragment_Bantuan_Detail secondFragtry = new Fragment_Bantuan_Detail();
            Bundle mBundle = new Bundle();
            mBundle.putString(KEY_ID, tvbantuanidlist.getText().toString());
            mBundle.putString(KEY_BANTUAN, tvbenatuan.getText().toString());
            mBundle.putString(KEY_PENANGANAN, tvbantuanpenangananlist.getText().toString());
//
            secondFragtry.setArguments(mBundle);
            FragmentManager fm = ((AppCompatActivity)context).getSupportFragmentManager();
            fm.beginTransaction().replace(R.id.flMain, secondFragtry).commit();
//            Toasty.success(context, "The Item Clicked is: " + getPosition(), Toast.LENGTH_SHORT).show();
            Toasty.success(context, "Next", Toast.LENGTH_SHORT).show();
        }
    }
}
