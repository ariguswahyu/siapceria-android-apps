package com.siapceria.rsudajibarang.siapceriaajb.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.siapceria.rsudajibarang.siapceriaajb.R;

public class SliderImageAdapter extends PagerAdapter {

    private Context context;
    private LayoutInflater layoutInflater;
    String [] mResIds = new String[]{"http://pngimg.com/upload/scratches_PNG6175.png","http://pngimg.com/upload/tiger_PNG549.png"};
    private Integer [] images = {R.drawable.siapceria2,R.drawable.background,R.drawable.siapceria2};

    public SliderImageAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return images.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {

        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.sliderimage, null);
        ImageView imageView = (ImageView) view.findViewById(R.id.imgslider);
        imageView.setImageResource(images[position]);

        ViewPager vp = (ViewPager) container;
        vp.addView(view, 0);
        return view;

    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

        ViewPager vp = (ViewPager) container;
        View view = (View) object;
        vp.removeView(view);

    }
}