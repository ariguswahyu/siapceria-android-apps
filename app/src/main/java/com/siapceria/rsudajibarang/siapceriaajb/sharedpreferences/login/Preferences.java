package com.siapceria.rsudajibarang.siapceriaajb.sharedpreferences.login;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class Preferences {

//    static final String KEY_USER_TEREGISTER ="user", KEY_PASS_TEREGISTER ="pass";
    static final String KEY_USERNAME_SEDANG_LOGIN = "Username_logged_in";
    static final String KEY_STATUS_SEDANG_LOGIN = "Status_logged_in";
    static final String KEY_JWT_TOKEN = "key_jwt_token";
    static final String KEY_USER_ID = "key_user_id";

    static final String KEY_USER_EMAIL = "key_user_email";
    static final String KEY_USER_NIK = "key_user_nik";
    static final String KEY_USER_NOHP = "key_user_nohp";


    private static SharedPreferences getSharedPreference(Context context){
        return PreferenceManager.getDefaultSharedPreferences(context);
    }


    public static void setLoggedInUser(Context context, String username){
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putString(KEY_USERNAME_SEDANG_LOGIN, username);
        editor.apply();
    }

    public static void setNikLoggedInUser(Context context, String nik){
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putString(KEY_USER_NIK, nik);
        editor.apply();
    }

    public static void setNOHPUser(Context context, String nohp){
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putString(KEY_USER_NOHP, nohp);
        editor.apply();
    }


    public static void setEmailLoggedInUser(Context context, String email){
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putString(KEY_USER_EMAIL, email);
        editor.apply();
    }

    public static String getNikLoggedInUser(Context context){
        return getSharedPreference(context).getString(KEY_USER_NIK,"");
    }



    public static String getEMailLoggedInUser(Context context){
        return getSharedPreference(context).getString(KEY_USER_EMAIL,"");
    }

    public static String getNOHPUser(Context context){
        return getSharedPreference(context).getString(KEY_USER_NOHP,"");
    }


    public static void setLoginToken(Context context, String token){
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putString(KEY_JWT_TOKEN, token);
        editor.apply();
    }
    public static void setUserId(Context context, String id){
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putString(KEY_USER_ID, id);
        editor.apply();
    }


    public static String getUserId(Context context){
        return getSharedPreference(context).getString(KEY_USER_ID,"");
    }


    public static String getLoginToken(Context context){
        return getSharedPreference(context).getString(KEY_JWT_TOKEN,"");
    }

    public static String getLoggedInUser(Context context){
        return getSharedPreference(context).getString(KEY_USERNAME_SEDANG_LOGIN,"");
    }

//    public static String getRegisteredUser(Context context){
//        return getSharedPreference(context).getString(KEY_USER_TEREGISTER,"");
//    }

    public static void setLoggedInStatus(Context context, boolean status){
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putBoolean(KEY_STATUS_SEDANG_LOGIN,status);
        editor.apply();
    }

    public static boolean getLoggedInStatus(Context context){
        return getSharedPreference(context).getBoolean(KEY_STATUS_SEDANG_LOGIN,false);
    }


}
