package com.siapceria.rsudajibarang.siapceriaajb.model.decodeToken;

import com.google.gson.annotations.SerializedName;

public class Response{

	@SerializedName("nik")
	private String nik;

	@SerializedName("firstname")
	private String firstname;

	@SerializedName("nohp")
	private String nohp;

	@SerializedName("id")
	private String id;

	@SerializedName("exp")
	private int exp;

	@SerializedName("iat")
	private int iat;

	@SerializedName("email")
	private String email;

	@SerializedName("lastname")
	private String lastname;

	@SerializedName("alamat")
	private String alamat;

	public void setNik(String nik){
		this.nik = nik;
	}

	public String getNik(){
		return nik;
	}

	public void setFirstname(String firstname){
		this.firstname = firstname;
	}

	public String getFirstname(){
		return firstname;
	}

	public void setNohp(String nohp){
		this.nohp = nohp;
	}

	public String getNohp(){
		return nohp;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setExp(int exp){
		this.exp = exp;
	}

	public int getExp(){
		return exp;
	}

	public void setIat(int iat){
		this.iat = iat;
	}

	public int getIat(){
		return iat;
	}

	public void setEmail(String email){
		this.email = email;
	}

	public String getEmail(){
		return email;
	}

	public void setLastname(String lastname){
		this.lastname = lastname;
	}

	public String getLastname(){
		return lastname;
	}

	public void setAlamat(String alamat){
		this.alamat = alamat;
	}

	public String getAlamat(){
		return alamat;
	}

	@Override
 	public String toString(){
		return 
			"Response{" + 
			"nik = '" + nik + '\'' + 
			",firstname = '" + firstname + '\'' + 
			",nohp = '" + nohp + '\'' + 
			",id = '" + id + '\'' + 
			",exp = '" + exp + '\'' + 
			",iat = '" + iat + '\'' + 
			",email = '" + email + '\'' + 
			",lastname = '" + lastname + '\'' + 
			",alamat = '" + alamat + '\'' + 
			"}";
		}
}