package com.siapceria.rsudajibarang.siapceriaajb.fragment.Jadwal;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.siapceria.rsudajibarang.siapceriaajb.R;
import com.siapceria.rsudajibarang.siapceriaajb.adapter.JadwalDokterDetailAdapter;
import com.siapceria.rsudajibarang.siapceriaajb.helper.ServiceGenerator;
import com.siapceria.rsudajibarang.siapceriaajb.indexActivity;
import com.siapceria.rsudajibarang.siapceriaajb.model.mJadwalDetail;
import com.siapceria.rsudajibarang.siapceriaajb.service.RestServices;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class JadwalDetailFragment extends Fragment {

    public static String KEY_KDDOKTER = "kddokter";
    public static String KEY_NAMADOKTER = "NAMADOKTER";
    public static String KEY_NIP = "nip";
    public static String KEY_NOTELP = "no_telp";

    private ArrayList<mJadwalDetail> goodModelJadwalArrayList;

    public static ProgressDialog pDialog;

    String getkddokter, getnamadokter, getnip, getnotelp, token;
    TextView nmdokter,nipdokter,notelpdokter,tvkosong;
    RecyclerView list_jadwal_praktek;

    public JadwalDetailFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_jadwal_detail, container, false);

        getkddokter = getArguments().getString(KEY_KDDOKTER);
        getnamadokter = getArguments().getString(KEY_NAMADOKTER);
        getnip = getArguments().getString(KEY_NIP);
        getnotelp = getArguments().getString(KEY_NOTELP);

        nmdokter =view.findViewById(R.id.nama_dokter);
        nipdokter = view.findViewById(R.id.keterangan_dokter);
        notelpdokter = view.findViewById(R.id.notelpdokter);
        tvkosong = view.findViewById(R.id.tvkosong);
        list_jadwal_praktek = view.findViewById(R.id.list_jadwal_praktek);
        RecyclerView.LayoutManager gridlay;
        gridlay = new GridLayoutManager(getActivity(), 1);
        list_jadwal_praktek.setLayoutManager(gridlay);

        Toolbar toolbar = view.findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        toolbar.setTitle("Back");
        toolbar.setTitleTextColor(getResources().getColor(android.R.color.white));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @SuppressLint("WrongConstant")
            @Override
            public void onClick(View v) {
//                Toasty.error(getActivity(), "Pilih Tanggal Control", Toast.LENGTH_LONG).show();
                JadwalFragment secondFragtry = new JadwalFragment();
                FragmentManager fm = getActivity().getSupportFragmentManager();
                fm.beginTransaction().replace(R.id.flMain, secondFragtry).commit();
            }
        });

        nmdokter.setText(getnamadokter);
        nipdokter.setText(getnip);
        notelpdokter.setText(getnotelp);
        token = indexActivity.getToken();

        getdata();
        return view;
    }

    private void getdata() {
//        pDialog = new ProgressDialog(getContext());
//        pDialog.setCancelable(false);
//        pDialog.setMessage("Loading ...");
//        showDialog();
        String kddokter = getArguments().getString(KEY_KDDOKTER);
        Log.i("TAGI", kddokter);
        // REST LOGIN ------------------------------------------------------------------
        RestServices restServices = ServiceGenerator.build().create(RestServices.class);
        Call poli = restServices.ListJadwalDokter(kddokter,"Bearer "+token);

        poli.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call call, Response response) {
                Log.i("TAGI", "jadwaldetail");
                try {
                    Log.i("TAGI", response.body().toString());
                    //Toast.makeText()
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            Log.i("onSuccess", response.body().toString());

                            String jsonresponse = response.body().toString();
                            writeListView(jsonresponse);
//                        hideDialog();
                        } else {
                            tvkosong.setText("Tidak ada jadwal praktek");
                            Log.i("onEmptyResponse", "Returned empty response");//Toast.makeText(getContext(),"Nothing returned",Toast.LENGTH_LONG).show();
//                        hideDialog();
                        }
                    }
                }
                catch (Exception ex){
                    tvkosong.setVisibility(View.VISIBLE);
                    tvkosong.setText("Tidak ada jadwal praktek");
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                tvkosong.setText("Tidak ada jadwal praktek");
                Log.i("onFailure",t.getMessage().toString());
//                hideDialog();
            }
        });
    }

    private void writeListView(String response){

        try {
            //getting the whole json object from the response
            JSONObject obj = new JSONObject(response);
            JSONObject rrrr = obj.getJSONObject("response");

            goodModelJadwalArrayList = new ArrayList<>();
            JSONArray dataArray  = rrrr.getJSONArray("dokterjaga");
            for (int i = 0; i < dataArray.length(); i++) {

                mJadwalDetail modelListView = new mJadwalDetail();
                JSONObject dataobj = dataArray.getJSONObject(i);

                modelListView.setId(dataobj.getString("id"));
                modelListView.setTanggal(dataobj.getString("tanggal"));
                modelListView.setNama_dokter(dataobj.getString("nama_dokter"));
                modelListView.setKddokter(dataobj.getString("kddokter"));
                modelListView.setPoly(dataobj.getString("poly"));

                goodModelJadwalArrayList.add(modelListView);

            }
            JadwalDokterDetailAdapter riwayatAdapter = new JadwalDokterDetailAdapter(getActivity(), goodModelJadwalArrayList);
            list_jadwal_praktek.setAdapter(riwayatAdapter);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}
