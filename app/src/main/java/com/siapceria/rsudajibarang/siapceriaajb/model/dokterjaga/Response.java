package com.siapceria.rsudajibarang.siapceriaajb.model.dokterjaga;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class Response{

	@SerializedName("dokterjaga")
	private List<DokterjagaItem> dokterjaga;

	public List<DokterjagaItem> getDokterjaga(){
		return dokterjaga;
	}
}