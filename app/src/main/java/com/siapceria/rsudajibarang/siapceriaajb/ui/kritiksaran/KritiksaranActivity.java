package com.siapceria.rsudajibarang.siapceriaajb.ui.kritiksaran;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.siapceria.rsudajibarang.siapceriaajb.R;

import butterknife.ButterKnife;

public class KritiksaranActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kritiksaran);
        ButterKnife.bind(this);
    }
}