package com.siapceria.rsudajibarang.siapceriaajb.fragment.Pendaftaran;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;
import com.siapceria.rsudajibarang.siapceriaajb.R;
import com.siapceria.rsudajibarang.siapceriaajb.fragment.HomeFragment;
import com.siapceria.rsudajibarang.siapceriaajb.helper.ServiceGenerator;
import com.siapceria.rsudajibarang.siapceriaajb.indexActivity;
import com.siapceria.rsudajibarang.siapceriaajb.model.mPatient;
import com.siapceria.rsudajibarang.siapceriaajb.service.RestServices;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.siapceria.rsudajibarang.siapceriaajb.indexActivity.pDialog;

/**
 * A simple {@link Fragment} subclass.
 */
public class Fragment_Daftar_Selesai extends Fragment {
    private ArrayList<mPatient> goodModelPasienArrayList;

    List<String> valuePasien = new ArrayList<String>();
    List<String> valueTglLahir = new ArrayList<String>();
    List<String> valueNama = new ArrayList<String>();

    public static String KEY_JENIS_PASIEN = "jenis_pasien";
    public static String KEY_HUBUNGAN = "hubungan";
    public static String KEY_NORM = "norm";
    public static String KEY_TGLLAHIR = "tgl_lahir";
    public static String KEY_NOTELP = "notelp";
    public static String KEY_EMAIL = "email";
    public static String KEY_TANGGAL = "tanggal";
    public static String KEY_CARABAYAR = "carabayar";
    public static String KEY_CARABAYARNAMA = "carabayarnama";
    public static String KEY_BPJS = "bpjs";
    public static String KEY_RUJUKAN = "rujukan";
    public static String KEY_POLI = "poliklinik";
    public static String KEY_POLINAMA = "polikliniknama";
    public static String KEY_DOKTER = "dokter";
    public static String KEY_DOKTERNAMA = "dokternama";
    public static String KEY_BOOKINGCODE = "bookingcode";
    public static String KEY_NAMA = "nama";
    public static String KEY_NIK = "nik";

    String getjenispasien,gethubungan,getnorm,gettgllahir,getnotelp,getemail,gettanggal,getcarabayar,getbpjs,getrujukan,getpoli,
            token,getcarabayarnama,getpolinama,getdokter,getdokternama, all, getbookingcode,getnama,getnik;
    TextView tvdaftarselesaitanggal,tvdaftarselesaipoli,tvdaftarselesaijeniskodebooking,tvdaftarselesainik,tvdaftarselesainamapasien,
            tvdaftarselesaipenjamin,tvdaftarselesainobpjs,tvdaftarselesaitl,tvdaftarselesainorm,tvdaftarselesaipenjaminid,tvdaftarselesaijenis;
    Button tvdaftarselesaijenispembayaran;

    public ImageView image;
    MultiFormatWriter multiFormatWriter = new MultiFormatWriter();

    public Fragment_Daftar_Selesai() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_daftar_selesai, container, false);
        getjenispasien = getArguments().getString(KEY_JENIS_PASIEN);
        gethubungan = getArguments().getString(KEY_HUBUNGAN);
        getnorm = getArguments().getString(KEY_NORM);
        gettgllahir = getArguments().getString(KEY_TGLLAHIR);
        getnotelp = getArguments().getString(KEY_NOTELP);
        getemail = getArguments().getString(KEY_EMAIL);
        gettanggal = getArguments().getString(KEY_TANGGAL);
        getcarabayar = getArguments().getString(KEY_CARABAYAR);
        getcarabayarnama = getArguments().getString(KEY_CARABAYARNAMA);
        getbpjs = getArguments().getString(KEY_BPJS);
        getrujukan = getArguments().getString(KEY_RUJUKAN);
        getpoli = getArguments().getString(KEY_POLI);
        getpolinama = getArguments().getString(KEY_POLINAMA);
        getdokter = getArguments().getString(KEY_DOKTER);
        getdokternama = getArguments().getString(KEY_DOKTERNAMA);
        getbookingcode = getArguments().getString(KEY_BOOKINGCODE);
        getnama = getArguments().getString(KEY_NAMA);
        getnik = getArguments().getString(KEY_NIK);
        token = indexActivity.getToken();

        tvdaftarselesaijenis = (TextView) view.findViewById(R.id.tvdaftarselesaijenis);
        tvdaftarselesaipenjaminid = (TextView) view.findViewById(R.id.tvdaftarselesaipenjaminid);
        tvdaftarselesaitanggal = (TextView) view.findViewById(R.id.tvdaftarselesaitanggal);
        tvdaftarselesaipoli = (TextView) view.findViewById(R.id.tvdaftarselesaipoli);
        tvdaftarselesaijenispembayaran = (Button) view.findViewById(R.id.tvdaftarselesaijenispembayaran);
        tvdaftarselesaijeniskodebooking = (TextView) view.findViewById(R.id.tvdaftarselesaijeniskodebooking);
        tvdaftarselesaipoli = (TextView) view.findViewById(R.id.tvdaftarselesaipoli);
        tvdaftarselesainik = (TextView) view.findViewById(R.id.tvdaftarselesainik);
        tvdaftarselesainamapasien = (TextView) view.findViewById(R.id.tvdaftarselesainamapasien);
        tvdaftarselesaipenjamin = (TextView) view.findViewById(R.id.tvdaftarselesaipenjamin);
        tvdaftarselesainobpjs = (TextView) view.findViewById(R.id.tvdaftarselesainobpjs);
        tvdaftarselesaitl = (TextView) view.findViewById(R.id.tvdaftarselesaitl);
        tvdaftarselesainorm = (TextView) view.findViewById(R.id.tvdaftarselesainorm);
        tvdaftarselesaijenis.setText(getjenispasien);
        tvdaftarselesaipenjaminid.setText(getcarabayar);

        tampil();

        image = (ImageView)view.findViewById(R.id.imageviewBCDaftarSelesai);

        barcode();

        view.findViewById(R.id.btnselesaidaftarselesai).setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                Selesai();
            }
        });

        Toolbar toolbar = view.findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        toolbar.setTitle("Back");
        toolbar.setTitleTextColor(getResources().getColor(android.R.color.white));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @SuppressLint("WrongConstant")
            @Override
            public void onClick(View v) {
                Selesai();
            }
        });
        return view;
    }


    public void barcode() {
        String all;
        all = getjenispasien+getnorm+gethubungan+gettgllahir+getnotelp+getemail+gettanggal+getcarabayarnama+getbpjs+getrujukan+getpolinama+getdokternama;
        try {
            BitMatrix bitMatrix = multiFormatWriter.encode(getbookingcode, BarcodeFormat.QR_CODE, 300, 300);
            BarcodeEncoder encoder = new BarcodeEncoder();
            Bitmap bitmap = encoder.createBitmap(bitMatrix);
            image.setImageBitmap(bitmap);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void tampil(){
        String jenispasien;
        jenispasien = getjenispasien;
        Log.d("OBJEK", getjenispasien);
        Log.d("OBJEK", getcarabayar);
        if(jenispasien == "0") {
            tvdaftarselesaitanggal.setText(gettanggal);
            tvdaftarselesaipoli.setText(getpolinama);
            tvdaftarselesaijenispembayaran.setText(getcarabayarnama);
            tvdaftarselesaijeniskodebooking.setText(getbookingcode);
            tvdaftarselesaipenjamin.setText(getcarabayarnama);
            tvdaftarselesainobpjs.setText(getbpjs);
            tvdaftarselesaitl.setText(gettgllahir);
            tvdaftarselesainik.setText(getnik);
            tvdaftarselesainorm.setText(getnorm);
            fetchJSONValidasiPasienLama();
        }
        if(jenispasien == "1"){
            tvdaftarselesaitanggal.setText(gettanggal);
            tvdaftarselesaipoli.setText(getpolinama);
            tvdaftarselesaijenispembayaran.setText(getcarabayarnama);
            tvdaftarselesaijeniskodebooking.setText(getbookingcode);
            tvdaftarselesaipenjamin.setText(getcarabayarnama);
            tvdaftarselesainobpjs.setText(getbpjs);
            tvdaftarselesaitl.setText(gettgllahir);
            tvdaftarselesainamapasien.setText(getnama);
            tvdaftarselesainik.setText(getnik);
        }
    }

    private void fetchJSONValidasiPasienLama(){
        pDialog = new ProgressDialog(getContext());
        pDialog.setCancelable(false);
        pDialog.setMessage("Logging in ...");
        showDialog();
        // REST LOGIN ------------------------------------------------------------------
        RestServices restServices = ServiceGenerator.build().create(RestServices.class);
        Call getpatient = restServices.detailPasien(getnorm,gettgllahir,"Bearer "+token);

        getpatient.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        hideDialog();
                        Log.i("onSuccess", response.body().toString());

                        String jsonresponse = response.body().toString();
                        spinJSONValidasiPasienLama(jsonresponse);
                        Log.i("onSuccess", response.body().toString());
                    } else {
                        hideDialog();
                        Log.i("onEmptyResponse", "Returned empty response");//Toast.makeText(getContext(),"Nothing returned",Toast.LENGTH_LONG).show();
                        Toasty.error(getActivity(), "Pasien belum terdaftar", Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                hideDialog();
                Log.i("onFailure",t.getMessage().toString());
                Toasty.error(getActivity(), "Pasien belum terdaftar", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void spinJSONValidasiPasienLama(String response){
        try {
            JSONObject obj = new JSONObject(response);
            JSONObject rrrr = obj.getJSONObject("response");
            JSONObject patient = rrrr.getJSONObject("patient");
            Log.i("Nama",patient.getString("NAMA"));
            tvdaftarselesainamapasien.setText(patient.getString("NAMA"));

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void Selesai() {
        // TODO Auto-generated method stub
        HomeFragment secondFragtry = new HomeFragment();
        FragmentManager fm = getActivity().getSupportFragmentManager();
        fm.beginTransaction().replace(R.id.flMain, secondFragtry).commit();
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

}
