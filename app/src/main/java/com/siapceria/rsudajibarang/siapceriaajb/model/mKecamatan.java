package com.siapceria.rsudajibarang.siapceriaajb.model;

public class mKecamatan {
    private String idkecamatan;
    private String namakecamatan;

    public String getIdkecamatan() {
        return idkecamatan;
    }

    public void setIdkecamatan(String idkecamatan) {
        this.idkecamatan = idkecamatan;
    }

    public String getNamakecamatan() {
        return namakecamatan;
    }

    public void setNamakecamatan(String namakecamatan) {
        this.namakecamatan = namakecamatan;
    }
}
