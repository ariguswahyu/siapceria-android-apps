package com.siapceria.rsudajibarang.siapceriaajb;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.siapceria.rsudajibarang.siapceriaajb.adapter.SliderImageAdapter;
import com.siapceria.rsudajibarang.siapceriaajb.constant.Base;
import com.siapceria.rsudajibarang.siapceriaajb.helper.ServiceGenerator;
import com.siapceria.rsudajibarang.siapceriaajb.model.decodeToken.ResponseDecodeToken;
import com.siapceria.rsudajibarang.siapceriaajb.model.user.LoginResponseRepos;
import com.siapceria.rsudajibarang.siapceriaajb.network.NetworkClient;
import com.siapceria.rsudajibarang.siapceriaajb.service.RestServices;
import com.siapceria.rsudajibarang.siapceriaajb.sharedpreferences.login.Preferences;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Login extends AppCompatActivity {

    public final static String TAG_TOKEN = "token";
    public static final String session_status = "session_status";
    public static final String my_shared_preferences = "my_shared_preferences";
    public static ProgressDialog pDialog;

    String token;
    TextView usernamea, passwordd;
    SharedPreferences sharedpreferences;
    Boolean session = false;
    RestServices restServices = ServiceGenerator.build().create(RestServices.class);
    Gson _messageGson = new Gson();
    LoginResponseRepos _loginResponseReposMessage;

    String versionApp = BuildConfig.VERSION_NAME;
    TextView textView;
    Snackbar snackbar;
    View snackBarView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        usernamea = findViewById(R.id.username);
        passwordd = findViewById(R.id.password);
        textView = findViewById(R.id.label_version_App);
        textView.setText("Versi : " + versionApp);


        sharedpreferences = getSharedPreferences(my_shared_preferences, Context.MODE_PRIVATE);
        session = sharedpreferences.getBoolean(session_status, false);
        token = sharedpreferences.getString(TAG_TOKEN, null);


        findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                login();
            }
        });
        findViewById(R.id.forgetpassword).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ForgetPassword();
            }
        });


    }

    public void Login(View v) {

        Intent i = new Intent(Login.this, indexActivity.class);
        startActivity(i);
    }

    public void Daftar(View v) {
        Intent i = new Intent(Login.this, daftarakun.class);
        startActivity(i);
    }

    private void ForgetPassword() {
        Intent i = new Intent(Login.this, forgetpassword.class);
        startActivity(i);
    }


    private void login() {

        String email = usernamea.getText().toString().trim();
        String password = passwordd.getText().toString().trim();
        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

        if (email.length() == 0 || password.length() == 0) {
            snackbar = Snackbar.make(findViewById(android.R.id.content), "username dan password harus dilengkapi", Snackbar.LENGTH_SHORT);
            snackBarView = snackbar.getView();
            snackBarView.setBackgroundColor(getResources().getColor(R.color.red_SIAPCERIA));
            snackbar.show();
        } else if (!email.matches(emailPattern)) {
            snackbar = Snackbar.make(findViewById(android.R.id.content), "Format Email salah", Snackbar.LENGTH_SHORT);
            snackBarView = snackbar.getView();
            snackBarView.setBackgroundColor(getResources().getColor(R.color.red_SIAPCERIA));
            snackbar.show();
        } else if (password.length() < 6) {
            snackbar = Snackbar.make(findViewById(android.R.id.content), "Password terlalu pendek", Snackbar.LENGTH_SHORT);
            snackBarView = snackbar.getView();
            snackBarView.setBackgroundColor(getResources().getColor(R.color.red_SIAPCERIA));
            snackbar.show();
        } else {
            pDialog = new ProgressDialog(this);
            pDialog.setCancelable(false);
            pDialog.setMessage(Base.LOADING_TEXT);
            showDialog();

            Call<LoginResponseRepos> login = restServices.Login(usernamea.getText().toString(), passwordd.getText().toString());
            login.enqueue(new Callback<LoginResponseRepos>() {
                @Override
                public void onResponse(Call<LoginResponseRepos> call, Response<LoginResponseRepos> response) {
                    if (response.isSuccessful()) {
                        if (response.code() == 200) {
                            hideDialog();
                            Preferences.setLoginToken(getApplicationContext(), response.body().getResponse().getToken());
                            Preferences.setLoggedInStatus(getApplicationContext(), true);
                            decodeToken();
                            SharedPreferences.Editor editor = sharedpreferences.edit();
                            editor.putBoolean(session_status, true);
                            editor.putString(TAG_TOKEN, response.body().getResponse().getToken());
                            editor.commit();
//                        // Memanggil main activity
//                            Intent intent = new Intent(Login.this, indexActivity.class);
                            Intent intent = new Intent(Login.this, SplashScreen.class);
                            intent.putExtra(TAG_TOKEN, response.body().getResponse().getToken());
                            finish();
                            startActivity(intent);

                        } else {
//                            snackbar = Snackbar.make(getWindow().getDecorView().getRootView(), "Periksa Koneksi Internet", Snackbar.LENGTH_SHORT);
                            snackbar = Snackbar.make(findViewById(android.R.id.content), "Periksa Koneksi Internet", Snackbar.LENGTH_SHORT);
                            snackBarView = snackbar.getView();
                            snackBarView.setBackgroundColor(getResources().getColor(R.color.red_SIAPCERIA));
                            snackbar.show();
                        }

                    } else {
                        hideDialog();
                        _loginResponseReposMessage = _messageGson.fromJson(response.errorBody().charStream(), LoginResponseRepos.class);
                        snackbar = Snackbar.make(findViewById(android.R.id.content), _loginResponseReposMessage.getMetaData().getMessage(), Snackbar.LENGTH_SHORT);
                        snackBarView = snackbar.getView();
                        snackBarView.setBackgroundColor(getResources().getColor(R.color.red_SIAPCERIA));
                        snackbar.show();
                    }
                }

                @Override
                public void onFailure(Call<LoginResponseRepos> call, Throwable t) {
                    hideDialog();
                    Log.d("checkLogin", t.getMessage());
                    snackbar = Snackbar.make(getWindow().getDecorView().getRootView(), "Silakan coba beberapa saat lagi", Snackbar.LENGTH_SHORT);
                    snackBarView = snackbar.getView();
                    snackBarView.setBackgroundColor(getResources().getColor(R.color.red_SIAPCERIA));
                    snackbar.show();
                }
            });
        }

    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    private void decodeToken() {

        String token = Preferences.getLoginToken(getBaseContext());

        Call<ResponseDecodeToken> decodeToken = NetworkClient.getInstance().getApiService().decodeToken(token);
        decodeToken.enqueue(new Callback<ResponseDecodeToken>() {
            @Override
            public void onResponse(Call<ResponseDecodeToken> call, Response<ResponseDecodeToken> response) {
                if (response.isSuccessful()) {
                    String id = response.body().getResponse().getId();
                    String email = response.body().getResponse().getEmail();
                    Preferences.setUserId(getApplicationContext(), response.body().getResponse().getId());
                } else {
                    // logout dan ke halaman login
                    Log.d("checkLogin", "elsee");
                }
            }

            @Override
            public void onFailure(Call<ResponseDecodeToken> call, Throwable t) {
                Log.d("checkLogin", t.getMessage());
                // logout dan ke halaman login
            }
        });

    }
}

