package com.siapceria.rsudajibarang.siapceriaajb.model.banner;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BannerResponse{

	@SerializedName("id")
	@Expose
	private String id;

	@SerializedName("judul")
	@Expose
	private String judul;

	@SerializedName("keterangan")
	@Expose
	private String keterangan;

	@SerializedName("gambar")
	@Expose
	private String gambar;


	public BannerResponse(String id, String judul, String keterangan, String gambar) {
		this.id = id;
		this.judul = judul;
		this.keterangan = keterangan;
		this.gambar = gambar;
	}

	public void setKeterangan(String keterangan){
		this.keterangan = keterangan;
	}

	public String getKeterangan(){
		return keterangan;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setJudul(String judul){
		this.judul = judul;
	}

	public String getJudul(){
		return judul;
	}

	public void setGambar(String gambar){
		this.gambar = gambar;
	}

	public String getGambar(){
		return gambar;
	}

	@Override
 	public String toString(){
		return 
			"BannerResponse{" + 
			"keterangan = '" + keterangan + '\'' + 
			",id = '" + id + '\'' + 
			",judul = '" + judul + '\'' + 
			",gambar = '" + gambar + '\'' + 
			"}";
		}
}