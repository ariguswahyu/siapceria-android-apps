package com.siapceria.rsudajibarang.siapceriaajb.model;

public class mRiwayat {
    private String id;
    private String pasienbaru;
    private String nomr;
    private String tanggal;
    private String poliklinik;
    private String dokter;
    private String jenis_pasien;
    private String hubungan;
    private String tgl_lahir;
    private String notelp;
    private String email;
    private String penjamin;
    private String nobpjs;
    private String norujukan;
    private String kodebooking;
    private String nama_title;
    private String isBoarded;



    private String noantrian;
    private String estmsilayanan;
    private String estimasipelayanan;



    public String getNoantrian() {
        return noantrian;
    }

    public void setNoantrian(String noantrian) {
        this.noantrian = noantrian;
    }

    public String getEstmsilayanan() {
        return estmsilayanan;
    }

    public void setEstmsilayanan(String estmsilayanan) {
        this.estmsilayanan = estmsilayanan;
    }

    public String getEstimasipelayanan() {
        return estimasipelayanan;
    }

    public void setEstimasipelayanan(String estimasipelayanan) {
        this.estimasipelayanan = estimasipelayanan;
    }




    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPasienbaru() {
        return pasienbaru;
    }

    public void setPasienbaru(String pasienbaru) {
        this.pasienbaru = pasienbaru;
    }

    public String getNomr() {
        return nomr;
    }

    public void setNomr(String nomr) {
        this.nomr = nomr;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getPoliklinik() {
        return poliklinik;
    }

    public void setPoliklinik(String poliklinik) {
        this.poliklinik = poliklinik;
    }

    public String getDokter() {
        return dokter;
    }

    public void setDokter(String dokter) {
        this.dokter = dokter;
    }

    public String getJenis_pasien() {
        return jenis_pasien;
    }

    public void setJenis_pasien(String jenis_pasien) {
        this.jenis_pasien = jenis_pasien;
    }

    public String getHubungan() {
        return hubungan;
    }

    public void setHubungan(String hubungan) {
        this.hubungan = hubungan;
    }

    public String getTgl_lahir() {
        return tgl_lahir;
    }

    public void setTgl_lahir(String tgl_lahir) {
        this.tgl_lahir = tgl_lahir;
    }

    public String getNotelp() {
        return notelp;
    }

    public void setNotelp(String notelp) {
        this.notelp = notelp;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPenjamin() {
        return penjamin;
    }

    public void setPenjamin(String penjamin) {
        this.penjamin = penjamin;
    }

    public String getKodebooking() {
        return kodebooking;
    }

    public void setKodebooking(String kodebooking) {
        this.kodebooking = kodebooking;
    }

    public String getNobpjs() {
        return nobpjs;
    }

    public void setNobpjs(String nobpjs) {
        this.nobpjs = nobpjs;
    }

    public String getNorujukan() {
        return norujukan;
    }

    public void setNorujukan(String norujukan) {
        this.norujukan = norujukan;
    }

    public String getNama_title() {
        return nama_title;
    }

    public void setNama_title(String nama_title) {
        this.nama_title = nama_title;
    }

    public String getIsBoarded() {
        return isBoarded;
    }

    public void setIsBoarded(String isBoarded) {
        this.isBoarded = isBoarded;
    }
}
