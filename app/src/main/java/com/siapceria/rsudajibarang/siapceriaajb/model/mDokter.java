package com.siapceria.rsudajibarang.siapceriaajb.model;

public class mDokter {
    private String kddokter;
    private String NAMADOKTER;
    private String nip;
    private String NAMAPOLI;
    private String no_telp;
    private String kuota;
    private String diambil;
    private String sex;


    public String getKddokter() {
        return kddokter;
    }

    public void setKddokter(String kddokter) {
        this.kddokter = kddokter;
    }

    public String getNAMADOKTER() {
        return NAMADOKTER;
    }

    public void setNAMADOKTER(String NAMADOKTER) {
        this.NAMADOKTER = NAMADOKTER;
    }

    public String getNip() {
        return nip;
    }

    public void setNip(String nip) {
        this.nip = nip;
    }

    public String getNo_telp() {
        return no_telp;
    }

    public void setNo_telp(String no_telp) {
        this.no_telp = no_telp;
    }

    public String getKuota() {
        return kuota;
    }

    public void setKuota(String kuota) {
        this.kuota = kuota;
    }

    public String getDiambil() {
        return diambil;
    }

    public void setDiambil(String diambil) {
        this.diambil = diambil;
    }

    public String getNAMAPOLI() {
        return NAMAPOLI;
    }

    public void setNAMAPOLI(String NAMAPOLI) {
        this.NAMAPOLI = NAMAPOLI;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }
}
