package com.siapceria.rsudajibarang.siapceriaajb;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.siapceria.rsudajibarang.siapceriaajb.constant.Base;
import com.siapceria.rsudajibarang.siapceriaajb.fragment.Bantuan.Fragment_Bantuan;
import com.siapceria.rsudajibarang.siapceriaajb.fragment.Riwayat.Fragment_Riwayat;
import com.siapceria.rsudajibarang.siapceriaajb.fragment.HomeFragment;
import com.siapceria.rsudajibarang.siapceriaajb.fragment.Profil.ProfilFragment;
import com.siapceria.rsudajibarang.siapceriaajb.sharedpreferences.login.Preferences;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.util.Log;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import es.dmoral.toasty.Toasty;

public class indexActivity extends AppCompatActivity {

    ConnectivityManager conMgr;
    private TextView mTextMessage;
    private Fragment fragment;
    private FragmentManager fragmentManager;

    public static final String TAG_TOKEN = "token";
    public static String token, tokennya;
    public static String iduser, idnya;
    private String url_insert = Base.URL + "auth/decode";
    public static ProgressDialog pDialog;
    public static String Errmsg;

    JSONArray JsonArrayProvinsi = null;
    String tag_json_obj = "json_obj_req";
    ArrayAdapter<String> spinnerAdapterProvinsi;
    List<String> valueidprovinsi = new ArrayList<String>();
    List<String> valuenamaprovinsi = new ArrayList<String>();

    private String url = Base.URL + "references/provinsi";

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    fragment = new HomeFragment();
                    break;
                case R.id.navigation_dashboard:
                    fragment = new Fragment_Riwayat();
                    break;
                case R.id.navigation_bantuan:
                    fragment = new Fragment_Bantuan();
                    break;
                case R.id.navigation_notifications:
                    fragment = new ProfilFragment();
//                    nama();
                    break;
            }
            final FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.replace(R.id.flMain, fragment).commit();
            return true;
        }
    };

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            // clear FLAG_TRANSLUCENT_STATUS flag:
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            // finally change the color
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.mainMenustatusBar));
        }


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_index);

        if (checkInternet()) {
            // ada koneksi internet

//            Toasty.success(indexActivity.this, "Ada", Toast.LENGTH_SHORT).show();

        } else {
            // tidak ada koneksi internet
            Toasty.error(indexActivity.this, "Tidak ada koneksi internet", Toast.LENGTH_SHORT).show();
        }

        mTextMessage = (TextView) findViewById(R.id.message);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.flMain, new HomeFragment()).commit();
        iduser = Preferences.getUserId(getApplicationContext());
        token = Preferences.getLoginToken(getApplicationContext());

//        onBackPressed();
    }

    private void getiduser() {
        String url;
        url = url_insert + "?token=" + getIntent().getStringExtra(TAG_TOKEN);

        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());

        StringRequest strReq = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    JSONObject obj = new JSONObject(response);
                    JSONObject employee = obj.getJSONObject("response");
                    iduser = employee.getString("id");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
//                Log.e(TAG, "Error: " + error.getMessage());
                Toast.makeText(indexActivity.this, "error", Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {

                // Posting parameters ke post url
                Map<String, String> params = new HashMap<String, String>();
                // jika id kosong maka simpan, jika id ada nilainya maka update

                params.put("token", getIntent().getStringExtra(TAG_TOKEN));

                return params;
            }
        };

        queue.add(strReq);

    }

    public static String getToken() {
        return token;
    }

    public static String getIdUser() {
        return iduser;
    }

    public void onBackPressed() {
        int fragments = getFragmentManager().getBackStackEntryCount();
        if (fragments == 1) {
            Toasty.info(indexActivity.this, "Tekan Sekali Lagi Untuk Keluar", Toast.LENGTH_SHORT).show();

        } else {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.flMain, new HomeFragment());
            ft.commit();
        }
        super.onBackPressed();
    }

    public boolean checkInternet() {
        boolean connectStatus = true;
        ConnectivityManager ConnectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = ConnectionManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected() == true) {
            connectStatus = true;
        } else {
            connectStatus = false;
        }
        return connectStatus;
    }

}
