package com.siapceria.rsudajibarang.siapceriaajb.fragment.tabjenispasien;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.siapceria.rsudajibarang.siapceriaajb.R;

import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.siapceria.rsudajibarang.siapceriaajb.adapter.RiwayatAdapter;
import com.siapceria.rsudajibarang.siapceriaajb.constant.Base;
import com.siapceria.rsudajibarang.siapceriaajb.fragment.Bantuan.Fragment_Bantuan;
import com.siapceria.rsudajibarang.siapceriaajb.fragment.Pendaftaran.Fragment_Dftronline;
import com.siapceria.rsudajibarang.siapceriaajb.helper.ServiceGenerator;
import com.siapceria.rsudajibarang.siapceriaajb.indexActivity;
import com.siapceria.rsudajibarang.siapceriaajb.model.mBooking;
import com.siapceria.rsudajibarang.siapceriaajb.model.mHubunganPasien;
import com.siapceria.rsudajibarang.siapceriaajb.model.mPatient;
import com.siapceria.rsudajibarang.siapceriaajb.model.mRiwayat;
import com.siapceria.rsudajibarang.siapceriaajb.service.RestServices;
import com.siapceria.rsudajibarang.siapceriaajb.sharedpreferences.login.Preferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.R.layout.simple_spinner_item;

/**
 * A simple {@link Fragment} subclass.
 */
public class Fragment_Pasien_Lama extends Fragment {
    public static String KEY_JENIS_PASIEN = "jenis_pasien";
    public static String KEY_HUBUNGAN = "hubungan";
    public static String KEY_NORM = "norm";
    public static String KEY_TGLLAHIR = "tgl_lahir";
    public static String KEY_NOTELP = "notelp";
    public static String KEY_EMAIL = "email";


    public static String KEY_NAMA = "nama";
    public static String KEY_NIK = "nik";
    public static String KEY_JENISKELAMIN = "jenis_kelamin";
    public static String KEY_TEMPATLAHIR = "tempat_lahir";
    public static String KEY_ALAMATSESUAIKTP = "alamat_sesuai_ktp";
    public static String KEY_PROVINSI = "provinsi";

    public static String KEY_KABUPATEN = "kabupaten";
    public static String KEY_KECAMATAN = "kecamatan";
    public static String KEY_KELURAHAN = "kelurahan";
    public static String KEY_NAMAAYAH = "nama_ayah";
    public static String KEY_NAMAIBU = "nama_ibu";
    public static String KEY_SUAMI = "nama_suami";

    public static String KEY_ISTRI = "nama_istri";
    public static String KEY_AGAMA = "agama";
    public static String KEY_PENDIDIKAN = "pendidikan";
    public static String KEY_PEKERJAAN = "pekerjaan";
    public static String KEY_STATUSKAWIN = "status_kawin";
    public static String KEY_KEWARGANEGARAAN = "kewarganegaraan";
    public static String KEY_SUKU = "suku";
    public static String KEY_BAHASADAERAH = "bahasa_daerah";
    public static String KEY_TITLE = "title";

    public static ProgressDialog pDialog;

    EditText plnorma, plnotelephona, plemaila, kalenderinputcatatan;
    private CheckBox cbDaftarRiwayat;
    Button btnpldaftara;
    ImageButton btnTanggal;
    String hubunganspinner, token, sharedemail, sharedNoHP;
    ;

    private Spinner sppbhubunganpasien, spfilter;

    private ArrayList<mHubunganPasien> goodModelHubunganPasienArrayList;
    private ArrayList<mRiwayat> goodModelRiwayatArrayList;
    private ArrayList<mPatient> goodModelPasienArrayList;
    private ArrayList<mBooking> goodModelBookingArrayList;


    private RiwayatAdapter riwayatAdapter;

    List<String> valueHubunganPasien = new ArrayList<String>();
    List<String> valuePasien = new ArrayList<String>();
    List<String> valueTglLahir = new ArrayList<String>();
    RecyclerView listView;

    List<String> valueId = new ArrayList<String>();
    List<String> valueNama = new ArrayList<String>();


    TextView tvtempnik, tvtempalamat, tvtemptempatlahir, tvtempnamaayah, tvtempnamaibu, tvtempsuami, tvtempistri, tvtempnotelp,
            tvtempprovinsi, tvtempkabupaten, tvtempkecamatan, tvtempkelurahan, tvjeniskelamin, tvagama, tvpendidikan, tvpekerjaan, tvstatus,
            tvtemptitle, tvtempnama, tv_riwayatPendaftaran, tvfilterriwayat, txketerangan;

    public Fragment_Pasien_Lama() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_pasien_lama, container, false);
        plnorma = (EditText) view.findViewById(R.id.plnorm);
        plnotelephona = (EditText) view.findViewById(R.id.plnotelephon);
        plemaila = (EditText) view.findViewById(R.id.plemail);
        sppbhubunganpasien = (Spinner) view.findViewById(R.id.spinnerplhubungan);
        spfilter = (Spinner) view.findViewById(R.id.spinnerplriwayat);
        btnpldaftara = (Button) view.findViewById(R.id.btnpldaftar);
        btnTanggal = (ImageButton) view.findViewById(R.id.btnTanggal1);
        kalenderinputcatatan = view.findViewById(R.id.kalenderinputcatatan);
        tvtempnik = view.findViewById(R.id.tvtempnik);
        tvtempalamat = view.findViewById(R.id.tvtempalamat);
        tvtemptempatlahir = view.findViewById(R.id.tvtemptempatlahir);
        tvtempnamaayah = view.findViewById(R.id.tvtempnamaayah);
        tvtempnamaibu = view.findViewById(R.id.tvtempnamaibu);
        tvtempsuami = view.findViewById(R.id.tvtempsuami);
        tvtempistri = view.findViewById(R.id.tvtempistri);
        tvtempnotelp = view.findViewById(R.id.tvtempnotelp);
        tvtempprovinsi = view.findViewById(R.id.tvtempprovinsi);
        tvtempkabupaten = view.findViewById(R.id.tvtempkabupaten);
        tvtempkecamatan = view.findViewById(R.id.tvtempkecamatan);
        tvtempkelurahan = view.findViewById(R.id.tvtempkelurahan);
        tvjeniskelamin = view.findViewById(R.id.tvjeniskelamin);
        tvagama = view.findViewById(R.id.tvagama);
        tvpendidikan = view.findViewById(R.id.tvpendidikan);
        tvpekerjaan = view.findViewById(R.id.tvpekerjaan);
        tvstatus = view.findViewById(R.id.tvstatus);
        tvtemptitle = view.findViewById(R.id.tvtemptitle);
        tvtempnama = view.findViewById(R.id.tvtempnama);
        cbDaftarRiwayat = (CheckBox) view.findViewById(R.id.cb_daftardaririwayat);
        tv_riwayatPendaftaran = (TextView) view.findViewById(R.id.tv_riwayatPendaftaran);
        tvfilterriwayat = (TextView) view.findViewById(R.id.tvfilterriwayat);

        txketerangan = view.findViewById(R.id.keterangan);

        sharedemail = Preferences.getEMailLoggedInUser(getActivity().getApplicationContext());
        sharedNoHP = Preferences.getNOHPUser(getActivity().getApplicationContext());
        plemaila.setText(sharedemail);
        plnotelephona.setText(sharedNoHP);

        spfilter.setVisibility(View.INVISIBLE);
        tv_riwayatPendaftaran.setVisibility(View.INVISIBLE);


        txketerangan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment_Bantuan secondFragtry = new Fragment_Bantuan();
                FragmentManager fm = getActivity().getSupportFragmentManager();
                fm.beginTransaction().replace(R.id.flMain, secondFragtry).commit();
            }
        });


        btnpldaftara.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validasi();

            }
        });

        btnTanggal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogDatePicker();
            }
        });

        cbDaftarRiwayat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cbDaftarRiwayat.isChecked()) {
                    plnorma.setText("");
                    kalenderinputcatatan.setText("");
                    fetchJSONRiwayatPendaftaran();
//                    Toast.makeText(getActivity(),"Checked!",Toast.LENGTH_SHORT).show();
                    spfilter.setVisibility(View.VISIBLE);
                    tv_riwayatPendaftaran.setVisibility(View.VISIBLE);
                    // HAPUS NORM DAN TGL

                } else {
//                    Toast.makeText(getActivity(),"Remove Checked!",Toast.LENGTH_SHORT).show();
                    spfilter.setVisibility(View.INVISIBLE);
                    tv_riwayatPendaftaran.setVisibility(View.INVISIBLE);
                    plnorma.setText("");
                    kalenderinputcatatan.setText("");
                    // HAPUS NORM DAN TGL
                }
            }
        });

        spfilter.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.i("spfilter", "onItemSelected");
                Log.i("spfilter", valueNama.get(position));
                tvfilterriwayat.setText(valueNama.get(position));
                getdata();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                tvfilterriwayat.setText("default");
                Log.i("spfilter", "onNothingSelected");
                getdata();
            }
        });

        token = Preferences.getLoginToken(getContext());
        fetchJSONHubunganPasien();
//        fetchJSONRiwayatPendaftaran();
        return view;
    }


    private void getdata() {
        Log.i("CEK_ID", tvfilterriwayat.getText().toString());
        Log.i("CEK_ID", "dari shared pref : " + Preferences.getUserId(getActivity().getApplicationContext()));


        String id = Preferences.getUserId(getActivity().getApplicationContext());
        // REST LOGIN ------------------------------------------------------------------
        RestServices restServices = ServiceGenerator.build().create(RestServices.class);
        Call riwayat = restServices.ListBookingDetail(id, tvfilterriwayat.getText().toString(), Base.PREFIX_AUTH + token);

        riwayat.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                try {
                    Log.i("DATATAAAA", response.body().toString());
                    //Toast.makeText()
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            Log.i("getdata", response.body().toString());
                            String jsonresponse = response.body().toString();
                            JSONObject obj = new JSONObject(jsonresponse);
                            JSONObject rrrr = obj.getJSONObject("response");
                            Log.i("writeListView", rrrr.toString());
                            JSONArray dataArray = rrrr.getJSONArray("bookinglist");
                            JSONObject object = dataArray.getJSONObject(0);

                            String nomr = object.getString("nomr");
                            String tgllahir = object.getString("tgllahir");
                            String nama_pasien = object.getString("nama_pasien");


                            if (TextUtils.isEmpty(nomr)) {
                                Toasty.warning(getActivity(), "Pendaftaran atas nama " + nama_pasien + ",  Sebelumnya Belum Selesai. Silakan ulangi daftar kembali  di pendaftaran pasien Baru", Toast.LENGTH_LONG).show();
                                plnorma.setText("");

                            } else {
                                plnorma.setText(nomr);

                            }

                            if (TextUtils.isEmpty(tgllahir)) {
                                Toasty.warning(getActivity(), "Silakan Perbaiki Tanggal Lahir untuk pasien atas nama  : " + nama_pasien, Toast.LENGTH_LONG).show();
                                kalenderinputcatatan.setText("");
                            } else {
                                kalenderinputcatatan.setText(tgllahir);

                            }


                        } else {
                            Log.i("getdata", "Returned empty response");
                            Toasty.warning(getActivity(), "Pasien yang anda pilih terdapat ketidakcocokan data, silakan ganti atau buwat pendaftaran baru", Toast.LENGTH_LONG).show();

                        }
                    }
                } catch (Exception ex) {

                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                Log.i("onFailure", t.getMessage().toString());
            }
        });
    }

    private void fetchJSONRiwayatPendaftaran() {
        pDialog = new ProgressDialog(getContext());
        pDialog.setCancelable(false);
        pDialog.setMessage("Logging in ...pp");
        showDialog();
        // REST LOGIN ------------------------------------------------------------------
        String id = Preferences.getUserId(getActivity().getApplicationContext());

        RestServices restServices = ServiceGenerator.build().create(RestServices.class);
        Call FilterBooking = restServices.FilterBooking(id, Base.PREFIX_AUTH + token);
        FilterBooking.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                hideDialog();
                if (response.isSuccessful()) {
                    if (response.code() == 200) {
//                        String jsonresponse = response.body().toString();
                        String jsonresponse = response.body().toString();

                        try {
                            JSONObject obj = new JSONObject(jsonresponse);
                            JSONObject rrrr = obj.getJSONObject("response");

                            if (TextUtils.isEmpty(rrrr.toString())) {
                                Toasty.error(getActivity(), "Data Gagal diterima, silakan coba lagi nanti", Toast.LENGTH_LONG).show();
                            }else{
                                spinJSONRiwayatPendaftaran(jsonresponse);
                            }

                        } catch (JSONException e) {
                            Toasty.error(getActivity(), "Data Gagal diterima, silakan coba lagi nanti", Toast.LENGTH_LONG).show();
                            e.printStackTrace();
                        }
//                        JSONObject rrrr = obj.getJSONObject("response");
//                        Log.i("onResponse", response.body().toString());
//                        Log.i("onSuccess", response.body().toString());
//                        String jsonresponse = response.body().toString();
//                        spinJSONRiwayatPendaftaran(jsonresponse);




                    }

                }

            }

            @Override
            public void onFailure(Call call, Throwable t) {
                hideDialog();
                Log.i("onFailure", t.getMessage().toString());
            }
        });
    }

    private void spinJSONRiwayatPendaftaran(String response) {

        try {

            JSONObject obj = new JSONObject(response);
            JSONObject rrrr = obj.getJSONObject("response");

            goodModelBookingArrayList = new ArrayList<>();
            JSONArray dataArray = rrrr.getJSONArray("bookinglist");

            for (int i = 0; i < dataArray.length(); i++) {

                mBooking spinnerModel = new mBooking();
                JSONObject dataobj = dataArray.getJSONObject(i);

                spinnerModel.setId(dataobj.getString("id"));
                spinnerModel.setNama_pasien(dataobj.getString("nama_pasien"));

                goodModelBookingArrayList.add(spinnerModel);

            }

//
            for (int i = 0; i < goodModelBookingArrayList.size(); i++) {
                valueId.add(goodModelBookingArrayList.get(i).getId().toString());
                valueNama.add(goodModelBookingArrayList.get(i).getNama_pasien().toString());
            }

            Log.i("spinJSONFilter", valueId.toString());
            Log.i("spinJSONFilter", valueNama.toString());

            ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getActivity(), simple_spinner_item, valueNama);
            Log.i("spinJSONFilter", spinnerArrayAdapter.toString());
            spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
            spfilter.setAdapter(spinnerArrayAdapter);
            spinnerArrayAdapter.notifyDataSetChanged();


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void validasi() {
        if (plnorma.getText().toString().length() == 0) {
            plnorma.setError("Masukkan No RM!");
            Toasty.error(getActivity(), "Isi Data Dengan Lengkap", Toast.LENGTH_LONG).show();
        }
        if (plnotelephona.getText().toString().length() == 0) {
            plnotelephona.setError("Masukkan No Telephon!");
            Toasty.error(getActivity(), "Isi Data Dengan Lengkap", Toast.LENGTH_LONG).show();
        }
        if (kalenderinputcatatan.getText().toString().length() == 0) {
            kalenderinputcatatan.setError("Pilih Tanggal Lahir!");
            Toasty.error(getActivity(), "Pilih Tanggal Lahir", Toast.LENGTH_LONG).show();
        }
        if (plemaila.getText().toString().length() == 0) {
            plemaila.setError("Masukkan Email!");
            Toasty.error(getActivity(), "Isi Data Dengan Lengkap", Toast.LENGTH_LONG).show();
        } else {
            fetchJSONValidasiPasien();
        }
    }

    private void nextFragment() {
        // TODO Auto-generated method stub
//        fetchJSONValidasiPasien();

        Fragment_Dftronline secondFragtry = new Fragment_Dftronline();
        Bundle mBundle = new Bundle();
        mBundle.putString(KEY_JENIS_PASIEN, "0");
        mBundle.putString(KEY_HUBUNGAN, sppbhubunganpasien.getSelectedItem().toString());
        mBundle.putString(KEY_NORM, plnorma.getText().toString());
        mBundle.putString(KEY_TGLLAHIR, kalenderinputcatatan.getText().toString());
        mBundle.putString(KEY_NOTELP, plnotelephona.getText().toString());
        mBundle.putString(KEY_EMAIL, plemaila.getText().toString());

        mBundle.putString(KEY_NAMA, tvtempnama.getText().toString());
        mBundle.putString(KEY_NIK, tvtempnik.getText().toString());
        mBundle.putString(KEY_TEMPATLAHIR, tvtemptempatlahir.getText().toString());
        mBundle.putString(KEY_ALAMATSESUAIKTP, tvtempalamat.getText().toString());
        mBundle.putString(KEY_NAMAAYAH, tvtempnamaayah.getText().toString());
        mBundle.putString(KEY_NAMAIBU, tvtempnamaibu.getText().toString());
        mBundle.putString(KEY_SUAMI, tvtempsuami.getText().toString());
        mBundle.putString(KEY_ISTRI, tvtempistri.getText().toString());
        mBundle.putString(KEY_PROVINSI, tvtempprovinsi.getText().toString());
        mBundle.putString(KEY_KABUPATEN, tvtempkabupaten.getText().toString());
        mBundle.putString(KEY_KECAMATAN, tvtempkecamatan.getText().toString());
        mBundle.putString(KEY_KELURAHAN, tvtempkelurahan.getText().toString());
        mBundle.putString(KEY_JENISKELAMIN, tvjeniskelamin.getText().toString());
        mBundle.putString(KEY_AGAMA, tvagama.getText().toString());
        mBundle.putString(KEY_PENDIDIKAN, tvpendidikan.getText().toString());
        mBundle.putString(KEY_PEKERJAAN, tvpekerjaan.getText().toString());
        mBundle.putString(KEY_STATUSKAWIN, tvstatus.getText().toString());
        mBundle.putString(KEY_SUKU, "-");
        mBundle.putString(KEY_BAHASADAERAH, "-");
        mBundle.putString(KEY_TITLE, tvtemptitle.getText().toString());

        secondFragtry.setArguments(mBundle);
        FragmentManager fm = getActivity().getSupportFragmentManager();
        fm.beginTransaction().replace(R.id.flMain, secondFragtry).commit();
    }

    private void fetchJSONHubunganPasien() {
        pDialog = new ProgressDialog(getContext());
        pDialog.setCancelable(false);
        pDialog.setMessage("Logging in ...oo");
//        showDialog();
        // REST LOGIN ------------------------------------------------------------------
        RestServices restServices = ServiceGenerator.build().create(RestServices.class);
        Call hubunganpasien = restServices.ListHubunganPasien(Base.PREFIX_AUTH + token);
        hubunganpasien.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                Log.i("Responsestring", response.body().toString());
                //Toast.makeText()
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        hideDialog();
                        Log.i("onSuccess", response.body().toString());

                        String jsonresponse = response.body().toString();
                        spinJSONHubunganPasien(jsonresponse);

                    } else {
                        hideDialog();
                        Log.i("onEmptyResponse", "Returned empty response");//Toast.makeText(getContext(),"Nothing returned",Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                hideDialog();
                Log.i("onFailure", t.getMessage().toString());
            }
        });
    }

    private void spinJSONHubunganPasien(String response) {

        try {

            JSONObject obj = new JSONObject(response);
            JSONObject rrrr = obj.getJSONObject("response");

            goodModelHubunganPasienArrayList = new ArrayList<>();
            JSONArray dataArray = rrrr.getJSONArray("hubungan");

            for (int i = 0; i < dataArray.length(); i++) {

                mHubunganPasien spinnerModel = new mHubunganPasien();
                JSONObject dataobj = dataArray.getJSONObject(i);

                spinnerModel.setNama(dataobj.getString("nama"));

                goodModelHubunganPasienArrayList.add(spinnerModel);

            }

            for (int i = 0; i < goodModelHubunganPasienArrayList.size(); i++) {
                valueHubunganPasien.add(goodModelHubunganPasienArrayList.get(i).getNama().toString());

            }

            ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getActivity(), simple_spinner_item, valueHubunganPasien);

            spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
            sppbhubunganpasien.setAdapter(spinnerArrayAdapter);
            spinnerArrayAdapter.notifyDataSetChanged();

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public String desimal(int des) {
        String dc = String.valueOf(des);
        if (des < 10) {
            dc = "0" + dc;
        }
        return dc;
    }

    public void alertDialogDatePicker() {
        // inflate file
        LayoutInflater inflater = (LayoutInflater) getActivity()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        // inflate file layout_datepicker.xml
        View view = inflater.inflate(R.layout.layout_datepicker, null, false);

        final Calendar calendar = Calendar.getInstance();
        DatePickerDialog dialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker arg0, int year, int month, int day_of_month) {
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, month);
                calendar.set(Calendar.DAY_OF_MONTH, day_of_month);
                String myFormat = "yyyy-MM-dd";
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());
                kalenderinputcatatan.setText(sdf.format(calendar.getTime()));
            }
        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        calendar.add(Calendar.DAY_OF_YEAR, -1);
//        dialog.getDatePicker().setMinDate(calendar.getTimeInMillis()); -(1000*60*60*24*1)// TODO: used to hide previous date,month and year
        calendar.add(Calendar.YEAR, 0);
        dialog.getDatePicker().setMaxDate((calendar.getTimeInMillis()));
        dialog.show();

    }

    private void fetchJSONValidasiPasien() {
        pDialog = new ProgressDialog(getContext());
        pDialog.setCancelable(false);
        pDialog.setMessage("Logging in ...");
        showDialog();
        // REST LOGIN ------------------------------------------------------------------
        RestServices restServices = ServiceGenerator.build().create(RestServices.class);
        Call getpatient = restServices.detailPasien(plnorma.getText().toString(), kalenderinputcatatan.getText().toString(), "Bearer " + token);

        getpatient.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                if (response.isSuccessful()) {
                    if (response.code() == 200) {
                        try {
                            JSONObject obj = new JSONObject(response.body().toString());
                            JSONObject metaData = obj.getJSONObject("metaData");
                            String code = metaData.getString("code");
                            hideDialog();
                            Log.i("onSuccess", code);
                            if ("200".equals(code)) {
                                String jsonresponse = response.body().toString();
                                spinJSONValidasiPasien(jsonresponse);
                                nextFragment();

                            } else {
                                Toasty.error(getActivity(), metaData.getString("message"), Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException e) {
                            hideDialog();
                            e.printStackTrace();
                        }

                    } else {
                        hideDialog();
                        Toasty.error(getActivity(), "Silakan dicoba lagi nanti", Toast.LENGTH_LONG).show();
                    }


//                    if (response.body() != null) {
//                        hideDialog();
//                        Log.i("onSuccess", response.body().toString());
//
//                        String jsonresponse = response.body().toString();
//                        spinJSONValidasiPasien(jsonresponse);
//
//                        Toasty.success(getActivity(), "Pilih Tanggal dan Jenis Pelayanan", Toast.LENGTH_LONG).show();
//                        nextFragment();
//                    } else {
//                        hideDialog();
//                        Log.i("onEmptyResponse", "Returned empty response");//Toast.makeText(getContext(),"Nothing returned",Toast.LENGTH_LONG).show();
//                        Toasty.error(getActivity(), "Pasien belum terdaftar", Toast.LENGTH_LONG).show();
//                    }
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                hideDialog();
                Log.i("onFailure", t.getMessage());
                Toasty.error(getActivity(), t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void spinJSONValidasiPasien(String response) {

        try {

            JSONObject obj = new JSONObject(response);
            JSONObject rrrr = obj.getJSONObject("response");
            JSONObject dataArray = rrrr.getJSONObject("patient");
            tvtempnik.setText(dataArray.getString("NOKTP"));
            tvtempnama.setText(dataArray.getString("NAMA"));
            tvtemptempatlahir.setText(dataArray.getString("TEMPAT"));
            tvtempalamat.setText(dataArray.getString("ALAMAT"));
            tvtemptempatlahir.setText(dataArray.getString("TEMPAT"));
            tvtempnamaayah.setText(dataArray.getString("nama_ayah"));
            tvtempnamaibu.setText(dataArray.getString("nama_ibu"));
            tvtempsuami.setText(dataArray.getString("nama_suami"));
            tvtempistri.setText(dataArray.getString("nama_istri"));
            tvtempprovinsi.setText(dataArray.getString("KDPROVINSI"));
            tvtempkabupaten.setText(dataArray.getString("KOTA"));
            tvtempkecamatan.setText(dataArray.getString("KDKECAMATAN"));
            tvtempkelurahan.setText(dataArray.getString("KELURAHAN"));
            tvjeniskelamin.setText(dataArray.getString("JENISKELAMIN"));
            tvagama.setText(dataArray.getString("AGAMA"));
            tvpendidikan.setText(dataArray.getString("PENDIDIKAN"));
            tvpekerjaan.setText(dataArray.getString("PEKERJAAN"));
            tvstatus.setText(dataArray.getString("STATUS"));
            tvtemptitle.setText(dataArray.getString("TITLE"));
            Log.i("onSuccess", dataArray.getString("TEMPAT"));

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }


}
