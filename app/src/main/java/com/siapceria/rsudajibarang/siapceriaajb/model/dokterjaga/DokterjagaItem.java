package com.siapceria.rsudajibarang.siapceriaajb.model.dokterjaga;

import com.google.gson.annotations.SerializedName;

public class DokterjagaItem{

	@SerializedName("diambil")
	private String diambil;

	@SerializedName("nip")
	private String nip;

	@SerializedName("kddokter")
	private String kddokter;

	@SerializedName("no_telp")
	private String noTelp;

	@SerializedName("NAMADOKTER")
	private String nAMADOKTER;

	@SerializedName("kuota")
	private String kuota;


	@SerializedName("NAMAPOLI")
	private String NAMAPOLI;

	@SerializedName("sex")
	private String sex;


	public String getDiambil() {
		return diambil;
	}


	public String getNAMAPOLI() {
		return NAMAPOLI;
	}

	public void setNAMAPOLI(String NAMAPOLI) {
		this.NAMAPOLI = NAMAPOLI;
	}

	public void setDiambil(String diambil) {
		this.diambil = diambil;
	}

	public String getNip() {
		return nip;
	}

	public void setNip(String nip) {
		this.nip = nip;
	}

	public String getKddokter() {
		return kddokter;
	}

	public void setKddokter(String kddokter) {
		this.kddokter = kddokter;
	}

	public String getNoTelp() {
		return noTelp;
	}

	public void setNoTelp(String noTelp) {
		this.noTelp = noTelp;
	}

	public String getnAMADOKTER() {
		return nAMADOKTER;
	}

	public void setnAMADOKTER(String nAMADOKTER) {
		this.nAMADOKTER = nAMADOKTER;
	}

	public String getKuota() {
		return kuota;
	}

	public void setKuota(String kuota) {
		this.kuota = kuota;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}
}