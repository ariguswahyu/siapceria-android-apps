package com.siapceria.rsudajibarang.siapceriaajb.model;

public class mBooking {
    private String id;
    private String pasienbaru;
    private String nomr;
    private String tanggal;
    private String bookingcode;
    private String NAMADOKTER;
    private String penjamin;
    private String nama_pasien;
    private String nama_title;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPasienbaru() {
        return pasienbaru;
    }

    public void setPasienbaru(String pasienbaru) {
        this.pasienbaru = pasienbaru;
    }

    public String getNomr() {
        return nomr;
    }

    public void setNomr(String nomr) {
        this.nomr = nomr;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getBookingcode() {
        return bookingcode;
    }

    public void setBookingcode(String bookingcode) {
        this.bookingcode = bookingcode;
    }

    public String getNAMADOKTER() {
        return NAMADOKTER;
    }

    public void setNAMADOKTER(String NAMADOKTER) {
        this.NAMADOKTER = NAMADOKTER;
    }

    public String getPenjamin() {
        return penjamin;
    }

    public void setPenjamin(String penjamin) {
        this.penjamin = penjamin;
    }

    public String getNama_pasien() {
        return nama_pasien;
    }

    public void setNama_pasien(String nama_pasien) {
        this.nama_pasien = nama_pasien;
    }

    public String getNama_title() {
        return nama_title;
    }

    public void setNama_title(String nama_title) {
        this.nama_title = nama_title;
    }
}
