package com.siapceria.rsudajibarang.siapceriaajb.fragment.Profil;

import android.annotation.SuppressLint;
import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.siapceria.rsudajibarang.siapceriaajb.R;
import com.siapceria.rsudajibarang.siapceriaajb.helper.ServiceGenerator;
import com.siapceria.rsudajibarang.siapceriaajb.indexActivity;
import com.siapceria.rsudajibarang.siapceriaajb.service.RestServices;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfilEditFragment extends Fragment {

    public static String KEY_NAMA = "nama";
    public static String KEY_NIK = "nik";
    public static String KEY_ALAMAT = "alamat";
    public static String KEY_NOHP = "nohp";

    EditText firstname, lastname,nik, alamat, nohp,email,password;
    String namaa, nika,alamata,nohpa;
    public ProfilEditFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profil_edit, container, false);
        firstname = (EditText) view.findViewById(R.id.etProfilEditFirstname);
        lastname = (EditText) view.findViewById(R.id.etProfilEditLastname);
        nik = (EditText) view.findViewById(R.id.etProfilEditNik);
        alamat = (EditText) view.findViewById(R.id.etProfilEditAlamat);
        nohp = (EditText) view.findViewById(R.id.etProfilEditNoHp);
        email = (EditText)view.findViewById(R.id.etProfilEditEmail);
        password = (EditText)view.findViewById(R.id.etProfilEditPassword);

        nika = getArguments().getString(KEY_NIK);
        namaa = getArguments().getString(KEY_NAMA);
        alamata = getArguments().getString(KEY_ALAMAT);
        nohpa = getArguments().getString(KEY_NOHP);

        nik.setText(nika);
        firstname.setText(namaa);
        alamat.setText(alamata);
        nohp.setText(nohpa);

        Toolbar toolbar = view.findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_baseline_arrow_back_24_black);
        toolbar.setTitle("Back");
        toolbar.setTitleTextColor(getResources().getColor(android.R.color.black));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @SuppressLint("WrongConstant")
            @Override
            public void onClick(View v) {
//                Toasty.error(getActivity(), "Pilih Tanggal Control", Toast.LENGTH_LONG).show();
                ProfilFragment secondFragtry = new ProfilFragment();
                FragmentManager fm = getActivity().getSupportFragmentManager();
                fm.beginTransaction().replace(R.id.flMain, secondFragtry).commit();
            }
        });

        view.findViewById(R.id.btnUpdateprofil).setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                Edit();
            }
        });
        return view;
    }

    private void Edit() {
        // TODO Auto-generated method stub
        updateProfile();
        ProfilFragment secondFragtry = new ProfilFragment();
        FragmentManager fm = getActivity().getSupportFragmentManager();
        fm.beginTransaction().replace(R.id.flMain, secondFragtry).commit();
    }

    private void updateProfile(){
//        String iduser = indexActivity.getIdUser();
        RestServices restServices = ServiceGenerator.build().create(RestServices.class);
        Call registrationNewUser = restServices.UpdateUser(indexActivity.getIdUser(), nohp.getText().toString(),
                alamat.getText().toString());

        registrationNewUser.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, retrofit2.Response response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {

//                        String jsonresponse = response.body().toString();

                        Log.i("onSuccess", response.body().toString());
                    } else {

                        Log.i("onEmptyResponse", "Returned empty response");
                    }
                }
                else {
                }

            }

            @Override
            public void onFailure(Call call, Throwable t) {
                Toasty.success(getActivity(), "GAGAL", Toast.LENGTH_LONG).show();
            }
        });
    }
}
