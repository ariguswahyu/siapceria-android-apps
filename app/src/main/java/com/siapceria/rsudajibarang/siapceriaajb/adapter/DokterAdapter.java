package com.siapceria.rsudajibarang.siapceriaajb.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.siapceria.rsudajibarang.siapceriaajb.R;
import com.siapceria.rsudajibarang.siapceriaajb.model.mDokter;

import java.util.ArrayList;

public class DokterAdapter extends RecyclerView.Adapter<DokterAdapter.MyViewHolder> {

    private LayoutInflater inflater;
    private ArrayList<mDokter> dataModelArrayList;
    public static String KEY_ID = "id";
    private final OnItemClickListener listener;

    public interface OnItemClickListener {
        void onItemClick(mDokter item, int posisi);
    }

    //, OnItemClickListener listener
    private Context context;
    public DokterAdapter(Context context, ArrayList<mDokter> dataModelArrayList, OnItemClickListener listener){

        this.context=context;
        inflater = LayoutInflater.from(context);
        this.dataModelArrayList = dataModelArrayList;
        this.listener = listener;
    }

    @Override
    public DokterAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.row_poli, parent, false);
        MyViewHolder holder = new MyViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(DokterAdapter.MyViewHolder holder, int position) {

//        holder.bind(dataModelArrayList.get(position), position, listener);
//        holder.posisi=position;
//        int kuota = Integer.parseInt(dataModelArrayList.get(position).getKuota());
//        int diambil = Integer.parseInt(dataModelArrayList.get(position).getDiambil());
//        int sisa = 0;
//        sisa = kuota-diambil;
//        holder.tvname.setText(dataModelArrayList.get(position).getNAMADOKTER());
//        holder.status.setText("Kuota : "+sisa);
        holder.bind(dataModelArrayList.get(position), position, listener);
        holder.posisi = position;
        holder.tvname.setText(dataModelArrayList.get(position).getNAMADOKTER());
        holder.tv_kuota.setText("Kuota Pasien : " + dataModelArrayList.get(position).getDiambil() + "/" + dataModelArrayList.get(position).getKuota());
        holder.tv_namapoliklinik.setText("Poliklinik " + dataModelArrayList.get(position).getNAMAPOLI());

        int diambill = Integer.parseInt(dataModelArrayList.get(position).getDiambil());
        int kuota = Integer.parseInt(dataModelArrayList.get(position).getKuota());

        String sex = dataModelArrayList.get(position).getSex();

        if (sex.equals("L")) {
            holder.img.setImageResource(R.drawable.male);
        }else{
            holder.img.setImageResource(R.drawable.female);
        }

        if (diambill != kuota) {
            holder.tx_status_kuota.setVisibility(View.GONE);
        } else {
            holder.tx_status_kuota.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public int getItemCount() {
        return dataModelArrayList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{

        public int posisi;
        TextView tvname, tv_kuota, tv_namapoliklinik, tx_status_kuota;
        ImageView img;

        public MyViewHolder(View itemView) {
            super(itemView);


            tvname = (TextView) itemView.findViewById(R.id.txt_poli);
            tv_kuota = (TextView) itemView.findViewById(R.id.txt_kuota);
            tv_namapoliklinik = (TextView) itemView.findViewById(R.id.all_menu_delivery_charge);
            tx_status_kuota = (TextView) itemView.findViewById(R.id.all_menu_deliverytime);
            img = (ImageView) itemView.findViewById(R.id.all_menu_image);
            itemView.setClickable(true);
        }

        public void bind(final mDokter item, final int position, final OnItemClickListener listener) {
            if(listener!=null) {
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
//                        Log.e("TAGI", String.valueOf(position));
//                        listener.onItemClick(item, position);
                        Log.e("OBJEK", String.valueOf(position));
//                        Log.e("OBJEK", tvname.getText().toString());
                        listener.onItemClick(item, position);
                    }
                });
            }
        }

    }
}
