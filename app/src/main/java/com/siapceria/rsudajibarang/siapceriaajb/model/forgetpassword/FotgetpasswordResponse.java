package com.siapceria.rsudajibarang.siapceriaajb.model.forgetpassword;

import com.google.gson.annotations.SerializedName;

public class FotgetpasswordResponse{

	@SerializedName("metaData")
	private MetaData metaData;

	@SerializedName("response")
	private Object response;

	public void setMetaData(MetaData metaData){
		this.metaData = metaData;
	}

	public MetaData getMetaData(){
		return metaData;
	}

	public void setResponse(Object response){
		this.response = response;
	}

	public Object getResponse(){
		return response;
	}

	@Override
 	public String toString(){
		return 
			"FotgetpasswordResponse{" + 
			"metaData = '" + metaData + '\'' + 
			",response = '" + response + '\'' + 
			"}";
		}
}