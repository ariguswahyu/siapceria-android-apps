package com.siapceria.rsudajibarang.siapceriaajb.service;

import com.orhanobut.hawk.Hawk;
import com.siapceria.rsudajibarang.siapceriaajb.model.user.LoginResponseRepos;

public class LocalService {
    private static final String LOGIN_RESPONSE = "Key.LoginResponse";

    public static void saveLogin(){
        delLogin();
        Hawk.put(LOGIN_RESPONSE, "data login");
    }

    public static LoginResponseRepos getLogin() {
        return Hawk.get(LOGIN_RESPONSE);
    }

    public static void delLogin(){
        Hawk.delete(LOGIN_RESPONSE);
    }


}
