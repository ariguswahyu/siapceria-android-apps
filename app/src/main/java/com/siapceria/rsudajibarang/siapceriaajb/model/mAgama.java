package com.siapceria.rsudajibarang.siapceriaajb.model;

public class mAgama {
    private String id;
    private String agama;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAgama() {
        return agama;
    }

    public void setAgama(String agama) {
        this.agama = agama;
    }
}
