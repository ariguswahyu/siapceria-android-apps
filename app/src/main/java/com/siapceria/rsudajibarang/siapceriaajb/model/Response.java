package com.siapceria.rsudajibarang.siapceriaajb.model;

import com.google.gson.annotations.SerializedName;

public class Response{

	@SerializedName("user")
	private User user;

	public void setUser(User user){
		this.user = user;
	}

	public User getUser(){
		return user;
	}

	@Override
 	public String toString(){
		return 
			"Response{" + 
			"user = '" + user + '\'' + 
			"}";
		}
}