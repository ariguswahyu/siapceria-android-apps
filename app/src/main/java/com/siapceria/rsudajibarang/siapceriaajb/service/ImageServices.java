package com.siapceria.rsudajibarang.siapceriaajb.service;

import com.siapceria.rsudajibarang.siapceriaajb.model.banner.BannerResponseRepos;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ImageServices {

    @GET("list")
    public Call<ArrayList<BannerResponseRepos>> tampilBannerHome();
}
