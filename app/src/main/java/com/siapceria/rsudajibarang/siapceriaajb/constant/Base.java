package com.siapceria.rsudajibarang.siapceriaajb.constant;

public class Base {
    public static final String BANNER_URL = "https://picsum.photos/v2/";
//
//    public static final String REST_BASE_URL = "http://10.0.2.2/";
//    public static final String URL = "http://10.0.2.2/api-simrs-rsudajibarang/api/";
//    public static final String REST_BASE_URL = "http://192.168.1.27:8090/";
//    public static final String URL = "http://192.168.1.27:8090/api-simrs-rsudajibarang/api/";
    // production
    public static final String REST_BASE_URL = "http://36.92.197.233:8090/";
    public static final String URL = "http://36.92.197.233:8090/api-simrs-rsudajibarang/api/";


    // Development
//    public static final String REST_BASE_URL = "http://192.168.1.27:8090/";
//    public static final String URL = "http://192.168.1.27:8090/api-simrs-rsudajibarang/api/";

    public static final String LOADING_TEXT = "Silakan Tunggu .....";

    public static final String WEBSITE_RSUDAJIBARANG = "http://rsudajibarang.banyumaskab.go.id/site";
    public static final String WEBSITE_BERITA_RSUDAJIBARANG = "http://rsudajibarang.banyumaskab.go.id/page/25401/profil-pejabat-struktural#.X7-GOWgzaUk";
    public static final String WEBSITE_INFORMASI_PUBLIK_RSUDAJIBARANG = "http://rsudajibarang.banyumaskab.go.id/page/25362/daftar-informasi-publik#.X8GzsmgzaUk";

    public static final String PREFIX_AUTH = "Bearer ";
    public static final String DEBUG_TEXT = "DEBUNGING";
    public static final String BACK_TEXT = "Back";
    //    //
}
