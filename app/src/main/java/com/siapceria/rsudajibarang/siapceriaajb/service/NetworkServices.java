package com.siapceria.rsudajibarang.siapceriaajb.service;

import com.siapceria.rsudajibarang.siapceriaajb.model.banner.BannerResponse;
import com.siapceria.rsudajibarang.siapceriaajb.model.decodeToken.ResponseDecodeToken;
import com.siapceria.rsudajibarang.siapceriaajb.model.userdetails.ResponseUserDetails;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Path;
import retrofit2.http.Query;


public interface NetworkServices {

    @GET("/api-simrs-rsudajibarang/api/references/banners")
    public Call<ResponseService<BannerResponse>> getBanner(@Header("Authorization") String auth);

    @GET("/api-simrs-rsudajibarang/api/auth/decode")
    public Call<ResponseDecodeToken> decodeToken(@Query("token") String token);

    @GET("/api-simrs-rsudajibarang/api/account/detailById/{id}")
    public Call<ResponseUserDetails> Account(@Path("id") String id, @Header("Authorization") String auth);

    @Headers("Accept:application/json")
    @GET("/api-simrs-rsudajibarang/api/account/detailById/{id}")
    Observable<ResponseUserDetails> AccountDetails(@Path("id") String id, @Header("Authorization") String auth);


}
