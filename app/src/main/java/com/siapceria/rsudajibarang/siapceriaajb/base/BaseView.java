package com.siapceria.rsudajibarang.siapceriaajb.base;

public interface BaseView<M> {
    void onSuccess(M model);
    void onError(String message);
}
