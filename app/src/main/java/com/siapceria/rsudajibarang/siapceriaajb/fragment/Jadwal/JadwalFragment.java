package com.siapceria.rsudajibarang.siapceriaajb.fragment.Jadwal;


import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.siapceria.rsudajibarang.siapceriaajb.R;
import com.siapceria.rsudajibarang.siapceriaajb.adapter.JadwalDokterAdapter;
import com.siapceria.rsudajibarang.siapceriaajb.adapter.PoliklinikAdapter;
import com.siapceria.rsudajibarang.siapceriaajb.adapter.RiwayatAdapter;
import com.siapceria.rsudajibarang.siapceriaajb.constant.Base;
import com.siapceria.rsudajibarang.siapceriaajb.fragment.HomeFragment;
import com.siapceria.rsudajibarang.siapceriaajb.helper.ServiceGenerator;
import com.siapceria.rsudajibarang.siapceriaajb.model.dokterjaga.ResponseDokterJaga;
import com.siapceria.rsudajibarang.siapceriaajb.model.mDokter;
import com.siapceria.rsudajibarang.siapceriaajb.model.mPoliklinik;
import com.siapceria.rsudajibarang.siapceriaajb.service.RestServices;
import com.siapceria.rsudajibarang.siapceriaajb.sharedpreferences.login.Preferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import devs.mulham.horizontalcalendar.HorizontalCalendar;
import devs.mulham.horizontalcalendar.HorizontalCalendarView;
import devs.mulham.horizontalcalendar.utils.HorizontalCalendarListener;
import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.CallAdapter;
import retrofit2.Callback;
import retrofit2.Response;

import static android.R.layout.simple_spinner_item;

/**
 * A simple {@link Fragment} subclass.
 */
public class JadwalFragment extends Fragment {

    public static String KEY_KDDOKTER = "kddokter";
    public static String KEY_NAMADOKTER = "NAMADOKTER";
    public static String KEY_NIP = "nip";
    public static String KEY_NOTELP = "no_telp";
    private ArrayList<mDokter> goodModelDokterArrayList;
    private JadwalDokterAdapter dokdokAdapter;
    public static ProgressDialog pDialog;

    RestServices serviceRequest;


    private ArrayList<mPoliklinik> goodModelArrayList;
    List<String> valueIdPoli = new ArrayList<String>();
    private ArrayList<String> playerNames = new ArrayList<String>();
    private Spinner spinnerpoli;
    private TextView tvtemppolii, tvtemppoliinama, tvtemppoliinip, tvtemppoliinotelp, infoumum;
    RecyclerView listView;
    String token, tanggal_selected;
    Calendar calendar;
    SimpleDateFormat simpleDateFormat;

    private HorizontalCalendar horizontalCalendar;

    ResponseDokterJaga _responseDokterJaga;

    public JadwalFragment() {
        // Required empty public constructor
    }


    @Override
    public void onStart() {
        super.onStart();
        serviceRequest = ServiceGenerator.build().create(RestServices.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_jadwal, container, false);
        calendar = Calendar.getInstance();
        simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        tanggal_selected = simpleDateFormat.format(calendar.getTime());

//        Toast.makeText(getContext(),  tanggal_selected, Toast.LENGTH_SHORT).show();


        /* starts before 1 month from now */
        Calendar startDate = Calendar.getInstance();
        startDate.add(Calendar.MONTH, -1);

        /* ends after 1 month from now */
        Calendar endDate = Calendar.getInstance();
        endDate.add(Calendar.MONTH, 1);

        horizontalCalendar = new HorizontalCalendar.Builder(view, R.id.calendarView)
                .range(startDate, endDate)
                .datesNumberOnScreen(5)
                .build();

        horizontalCalendar.setCalendarListener(new HorizontalCalendarListener() {
            @Override
            public void onDateSelected(Calendar date, int position) {
//                Toast.makeText(getContext(), DateFormat.getDateInstance().format(date) + " is selected!", Toast.LENGTH_SHORT).show();
                fetchJSONPoli();
                tanggal_selected = simpleDateFormat.format(date.getTime());
//                getActivePoliklinik(tanggal_selected);
//                Toast.makeText(getContext(),  tanggal_selected, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCalendarScroll(HorizontalCalendarView calendarView, int dx, int dy) {
                super.onCalendarScroll(calendarView, dx, dy);
//                Toast.makeText(getContext(), "deededed", Toast.LENGTH_SHORT).show();
            }


        });


        spinnerpoli = view.findViewById(R.id.spjadwaldokfilpoli);
        tvtemppolii = view.findViewById(R.id.tvtemppolii);
        infoumum = view.findViewById(R.id.infoumum);
//        infoumum.setText("Jadwal tanggal : " + formattedDate);
        tvtemppoliinama = view.findViewById(R.id.tvtemppoliinama);
        tvtemppoliinip = view.findViewById(R.id.tvtemppoliinip);
        tvtemppoliinotelp = view.findViewById(R.id.tvtemppoliinotelp);
        listView = (RecyclerView) view.findViewById(R.id.rcvjadwaldokter);
        token = Preferences.getLoginToken(getContext());


        RecyclerView.LayoutManager gridlay;
        gridlay = new GridLayoutManager(getActivity(), 1);
        listView.setLayoutManager(gridlay);



        Toolbar toolbar = view.findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        toolbar.setTitle("Back");
        toolbar.setTitleTextColor(getResources().getColor(android.R.color.white));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @SuppressLint("WrongConstant")
            @Override
            public void onClick(View v) {

                HomeFragment secondFragtry = new HomeFragment();
                FragmentManager fm = getActivity().getSupportFragmentManager();
                fm.beginTransaction().replace(R.id.flMain, secondFragtry).commit();
            }
        });


        spinnerpoli.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                tvtemppolii.setText(valueIdPoli.get(position));
                getdata();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });


        fetchJSONPoli();
        return view;
    }

    private void fetchJSONPoli() {
        pDialog = new ProgressDialog(getContext());
        pDialog.setCancelable(false);
        pDialog.setMessage(Base.LOADING_TEXT);
        showDialog();
        // REST LOGIN ------------------------------------------------------------------
        RestServices restServices = ServiceGenerator.build().create(RestServices.class);
        Call poli = restServices.ListPolik(Base.PREFIX_AUTH + token);

        poli.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {

                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        String jsonresponse = response.body().toString();
                        spinJSONPoli(jsonresponse);
                        hideDialog();
                    } else {
                        hideDialog();
                        Log.i("onEmptyResponse", "Returned empty response");//Toast.makeText(getContext(),"Nothing returned",Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                Log.i("onFailure", t.getMessage().toString());
                hideDialog();
            }
        });
    }

    private void spinJSONPoli(String response) {

        try {

            JSONObject obj = new JSONObject(response);
            JSONObject rrrr = obj.getJSONObject("response");

            goodModelArrayList = new ArrayList<>();
            JSONArray dataArray = rrrr.getJSONArray("poliklinik");

            for (int i = 0; i < dataArray.length(); i++) {

                mPoliklinik spinnerModel = new mPoliklinik();
                JSONObject dataobj = dataArray.getJSONObject(i);

                spinnerModel.setKode(dataobj.getString("kode"));
                spinnerModel.setNama(dataobj.getString("nama"));

                goodModelArrayList.add(spinnerModel);

            }

            for (int i = 0; i < goodModelArrayList.size(); i++) {
                valueIdPoli.add(goodModelArrayList.get(i).getKode().toString());
                playerNames.add(goodModelArrayList.get(i).getNama().toString());
            }

            ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getActivity(), simple_spinner_item, playerNames);

            spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
            spinnerpoli.setAdapter(spinnerArrayAdapter);
            spinnerArrayAdapter.notifyDataSetChanged();

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void getdata() {

        String polii = tvtemppolii.getText().toString();
        RestServices restServices = ServiceGenerator.build().create(RestServices.class);
        Call<ResponseDokterJaga> dokterjaga = restServices.ListDokterPoliklinik(polii, tanggal_selected, "Bearer " + token);

        dokterjaga.enqueue(new Callback<ResponseDokterJaga>() {
            @Override
            public void onResponse(Call<ResponseDokterJaga> call, Response<ResponseDokterJaga> response) {
                if (response.isSuccessful()) {
                    if (response.body().getResponse() != null) {

                        writeListView(response.body());
//                        hideDialog();
                    } else {
                        Toasty.warning(getActivity(), response.body().getMetaData().getMessage(), Toast.LENGTH_SHORT).show();
                        listView.setAdapter(null);
                    }

                } else {
                    // Toasty.warning(getActivity(), "Silakan Lakukan Logout dan coba lagi", Toast.LENGTH_LONG).show();
                }

                hideDialog();
            }

            @Override
            public void onFailure(Call<ResponseDokterJaga> call, Throwable t) {
                Toasty.error(getActivity(), "Silakan Muat Ulang Perangkat Anda", Toast.LENGTH_LONG).show();
            }
        });

    }


    private void writeListView(Object _responseDokterJaga) {

        try {

            Gson gson = new Gson();
            JSONObject obj = new JSONObject(gson.toJson(_responseDokterJaga));
//            Log.i("datas", obj.toString());
            JSONObject rrrr = obj.getJSONObject("response");

            goodModelDokterArrayList = new ArrayList<>();
            JSONArray dataArray = rrrr.getJSONArray("dokterjaga");
            for (int i = 0; i < dataArray.length(); i++) {

                mDokter modelListView = new mDokter();
                JSONObject dataobj = dataArray.getJSONObject(i);

                modelListView.setKddokter(dataobj.getString("kddokter"));
                modelListView.setNAMADOKTER(dataobj.getString("NAMADOKTER"));
                modelListView.setNip(dataobj.getString("nip"));
                modelListView.setKuota(dataobj.getString("kuota"));
                modelListView.setNo_telp(dataobj.getString("no_telp"));
                modelListView.setDiambil(dataobj.getString("diambil"));
                modelListView.setNAMAPOLI(dataobj.getString("NAMAPOLI"));
                modelListView.setSex(dataobj.getString("sex"));
                goodModelDokterArrayList.add(modelListView);

            }

            dokdokAdapter = new JadwalDokterAdapter(getContext(), goodModelDokterArrayList, new JadwalDokterAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(mDokter item, int posisi) {
//                    Log.i("jalan poli", String.valueOf(posisi));
//                    String poli, id, nama, nip, notelp;
//                    poli = "poli anak";
//
//
//                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
//                    // set title dialog
//                    alertDialogBuilder.setTitle("" + item.getNAMADOKTER() + "?");
//                    tvtemppolii.setText(item.getKddokter());
//                    tvtemppoliinama.setText(item.getNAMADOKTER());
//                    tvtemppoliinip.setText(item.getNip());
//                    tvtemppoliinotelp.setText(item.getNo_telp());
//                    // set pesan dari dialog
//
//
//
//
//                    JadwalDetailFragment secondFragtry = new JadwalDetailFragment();
//                    Bundle mBundle = new Bundle();
//                    mBundle.putString(KEY_KDDOKTER, tvtemppolii.getText().toString());
//                    mBundle.putString(KEY_NAMADOKTER, tvtemppoliinama.getText().toString());
//                    mBundle.putString(KEY_NIP, tvtemppoliinip.getText().toString());
//                    mBundle.putString(KEY_NOTELP, tvtemppoliinotelp.getText().toString());
//
//                    secondFragtry.setArguments(mBundle);
//                    FragmentManager fm = getActivity().getSupportFragmentManager();
//                    fm.beginTransaction().replace(R.id.flMain, secondFragtry).commit();


//                    alertDialogBuilder
//                            .setMessage(item.getNAMADOKTER())
////                            .setIcon(R.mipmap.ic_launcher)
//                            .setCancelable(false)
//                            .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
//                                public void onClick(DialogInterface dialog, int id) {
//                                    // jika tombol diklik, maka akan menutup activity ini
////                                    nextFragment();
//
//                                }
//                            })
//                            .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
//                                public void onClick(DialogInterface dialog, int id) {
//                                    // jika tombol ini diklik, akan menutup dialog
//                                    // dan tidak terjadi apa2
//                                    dialog.cancel();
//                                }
//                            });
//
//                    // membuat alert dialog dari builder
//                    AlertDialog alertDialog = alertDialogBuilder.create();
//
//                    // menampilkan alert dialog
//                    alertDialog.show();
                }

            });
            listView.setAdapter(dokdokAdapter);


        } catch (JSONException e) {
            //
        }


    }


    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}
