package com.siapceria.rsudajibarang.siapceriaajb.model.decodeToken;

import com.google.gson.annotations.SerializedName;

public class ResponseDecodeToken{

	@SerializedName("metaData")
	private MetaData metaData;

	@SerializedName("response")
	private Response response;

	public void setMetaData(MetaData metaData){
		this.metaData = metaData;
	}

	public MetaData getMetaData(){
		return metaData;
	}

	public void setResponse(Response response){
		this.response = response;
	}

	public Response getResponse(){
		return response;
	}

	@Override
 	public String toString(){
		return 
			"ResponseDecodeToken{" + 
			"metaData = '" + metaData + '\'' + 
			",response = '" + response + '\'' + 
			"}";
		}
}