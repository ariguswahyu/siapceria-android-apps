package com.siapceria.rsudajibarang.siapceriaajb.model;

public class mBantuan {
    private String id;
    private String bantuan;
    private String penanganan;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBantuan() {
        return bantuan;
    }

    public void setBantuan(String bantuan) {
        this.bantuan = bantuan;
    }

    public String getPenanganan() {
        return penanganan;
    }

    public void setPenanganan(String penanganan) {
        this.penanganan = penanganan;
    }
}
