package com.siapceria.rsudajibarang.siapceriaajb;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.snackbar.Snackbar;
import com.siapceria.rsudajibarang.siapceriaajb.constant.Base;
import com.siapceria.rsudajibarang.siapceriaajb.helper.ServiceGenerator;
import com.siapceria.rsudajibarang.siapceriaajb.model.forgetpassword.FotgetpasswordResponse;
import com.siapceria.rsudajibarang.siapceriaajb.service.RestServices;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;

public class forgetpassword extends AppCompatActivity {

    EditText emaile, nohpe;
    String codejson;
    Button sendforgot;
    private String url_insert = Base.URL + "auth/forgotpassword";
    public static ProgressDialog pDialog;
    int success;
    private static final String TAG_SUCCESS = "1";
    private static final String TAG_MESSAGE = "message";

    RestServices restServices = ServiceGenerator.build().create(RestServices.class);

    private static final String TAG = forgetpassword.class.getSimpleName();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgetpassword);

        emaile = (EditText) findViewById(R.id.fpemail);
//        nohpe = (EditText) findViewById(R.id.fpnohp);
        sendforgot = (Button) findViewById(R.id.btnsimpanlupapassword);

        sendforgot.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
//                forgetpassword();
//                simpan();
                sendRequestForgetPassword();
            }
        });
    }

    private void forgetpassword() {
        String url;
        url = url_insert;
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());

        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e("OBJECT", "Response");
                Log.d("OBJECT","SUKSESSS BROOO");
//                Toast.makeText(forgetpassword.this, "SUKSES", Toast.LENGTH_LONG).show();
                try {

                    JSONObject obj = new JSONObject(response);

                    JSONObject employee = obj.getJSONObject("metaData");
                    String message = employee.getString("message");
                    Log.d("OBJECT",message);
                    Toast.makeText(forgetpassword.this, message, Toast.LENGTH_LONG).show();

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error: " + error.getMessage());
                Toast.makeText(forgetpassword.this, "error", Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {

                // Posting parameters ke post url
                Map<String, String> params = new HashMap<String, String>();
                // jika id kosong maka simpan, jika id ada nilainya maka update

                params.put("email", emaile.getText().toString());
//                params.put("notelepon", nohpe.getText().toString());

                return params;
            }
        };

        queue.add(strReq);
//        AppController.getInstance().addToRequestQueue(strReq);

    }

    private void simpan() {
        String url;
        url = url_insert;
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());

        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e(TAG, "Response: " + response.toString());
                Toast.makeText(forgetpassword.this, "SUKSES", Toast.LENGTH_LONG).show();
                try {
                    JSONObject jObj = new JSONObject(response);
                    JSONObject rrrr = jObj.getJSONObject("response");
                    success = rrrr.getInt(TAG_SUCCESS);

                    // Cek error node pada json
                    if (success == 1) {
                        Log.d("Add/update", jObj.toString());
                        Toast.makeText(forgetpassword.this, jObj.getString(TAG_MESSAGE), Toast.LENGTH_LONG).show();

                    } else {
                        Toast.makeText(forgetpassword.this, jObj.getString(TAG_MESSAGE), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error: " + error.getMessage());
                Toast.makeText(forgetpassword.this, error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {

                // Posting parameters ke post url
                Map<String, String> params = new HashMap<String, String>();
                // jika id kosong maka simpan, jika id ada nilainya maka update

                params.put("email", emaile.getText().toString());
                //params.put("notelepon", nohpe.getText().toString());

                return params;
            }

        };

        queue.add(strReq);
//        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }

    private void sendRequestForgetPassword(){
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        pDialog.setMessage("Logging in ...");
        showDialog();

//        Call<LoginResponseRepos>  login= restServices.Login(usernamea.getText().toString(), passwordd.getText().toString());
        Call<FotgetpasswordResponse> forgetpassword = restServices.ForgetPassword(emaile.getText().toString());
//        forgetpassword.enqueue(new Callback<FotgetpasswordResponse>)
        forgetpassword.enqueue(new Callback<FotgetpasswordResponse>(){
            @Override
            public void onResponse(Call<FotgetpasswordResponse> call, retrofit2.Response<FotgetpasswordResponse> response) {
                hideDialog();
                int res_code = response.body().getMetaData().getCode();
                if(res_code==200){

                    Toasty.success(getApplicationContext(), response.body().getMetaData().getMessage().toString(), Toast.LENGTH_LONG).show();

                    new android.os.Handler().postDelayed(
                            new Runnable() {
                                public void run() {
                                    Intent intent = new Intent(forgetpassword.this, Login.class);
                            startActivity(intent);
                            finish();
                                }
                            }, 2000);
                }else{
                    Snackbar snackbar = Snackbar
                            .make(findViewById(R.id.halaman_forget_password), response.body().getMetaData().getMessage().toString(), Snackbar.LENGTH_LONG);
                    View snackBarView = snackbar.getView();
                    snackBarView.setBackgroundColor(Color.RED);
                    snackbar.show();
                }

            }

            @Override
            public void onFailure(Call<FotgetpasswordResponse> call, Throwable t) {
                hideDialog();
                Toasty.error(getApplicationContext(), "Gagal Menghubungkan ke server...", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}
