package com.siapceria.rsudajibarang.siapceriaajb.fragment;


import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager.widget.PagerAdapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.snackbar.Snackbar;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;

import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;
import com.jude.rollviewpager.RollPagerView;
import com.siapceria.rsudajibarang.siapceriaajb.R;
import com.siapceria.rsudajibarang.siapceriaajb.adapter.BannerAdapter;
import com.siapceria.rsudajibarang.siapceriaajb.constant.Base;
import com.siapceria.rsudajibarang.siapceriaajb.fragment.Jadwal.JadwalFragment;
import com.siapceria.rsudajibarang.siapceriaajb.fragment.Kritiksaran.KritikSaranFragment;
import com.siapceria.rsudajibarang.siapceriaajb.fragment.PelayananPublik.PelayananPublikFragment;
import com.siapceria.rsudajibarang.siapceriaajb.fragment.ProfileRumahSakit.ProfileRSFragment;
import com.siapceria.rsudajibarang.siapceriaajb.helper.BannerGenerator;
import com.siapceria.rsudajibarang.siapceriaajb.helper.ServiceGenerator;
import com.siapceria.rsudajibarang.siapceriaajb.indexActivity;
import com.siapceria.rsudajibarang.siapceriaajb.model.banner.BannerResponse;
import com.siapceria.rsudajibarang.siapceriaajb.model.banner.BannerResponseRepos;
import com.siapceria.rsudajibarang.siapceriaajb.model.mBooking;
import com.siapceria.rsudajibarang.siapceriaajb.network.NetworkClient;
import com.siapceria.rsudajibarang.siapceriaajb.service.ImageServices;
import com.siapceria.rsudajibarang.siapceriaajb.service.ResponseService;
import com.siapceria.rsudajibarang.siapceriaajb.service.RestServices;
import com.siapceria.rsudajibarang.siapceriaajb.sharedpreferences.login.Preferences;
import com.siapceria.rsudajibarang.siapceriaajb.ui.kritiksaran.KritiksaranActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;

import static com.siapceria.rsudajibarang.siapceriaajb.indexActivity.pDialog;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {

    @BindView(R.id.sliderhome)
    RollPagerView slider;

    public Button barcode;
    public ImageView image;
    public EditText txt_booking_nama, txt_booking_jaminan, txt_booking_dr, txt_booking_kode, txttgldaftarhome;
    public TextView nama, email;
    LinearLayout ll;
    RollPagerView viewPager;
    String token;

    String teksbarcode, firsname, lastname, emaile, nik, alamat, nohp, userid;
    private String url_insert = Base.URL + "auth/decode";

    MultiFormatWriter multiFormatWriter = new MultiFormatWriter();

    ImageServices _imageService = BannerGenerator.build().create(ImageServices.class);
    PagerAdapter mAdapter;


    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, view);

        nama = (TextView) view.findViewById(R.id.profil1);
        nama.setText("Hai, " + Preferences.getLoggedInUser(getContext()));
        email = (TextView) view.findViewById(R.id.NIK1);
        email.setText(Preferences.getEMailLoggedInUser(getContext()));
        teksbarcode = Preferences.getLoginToken(getContext());
        token = Preferences.getLoginToken(getContext());
//        namaa();
        showBanner();

        view.findViewById(R.id.tvpendaftaranpoli).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Daftar();
            }
        });
        view.findViewById(R.id.tvjadwaldokter).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Jadwal();
            }
        });

        view.findViewById(R.id.jadwaloperasi).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar snackbar;
                snackbar = Snackbar.make(view, "Menu ini akan segera hadir", Snackbar.LENGTH_SHORT);
                View snackBarView = snackbar.getView();
                snackBarView.setBackgroundColor(getResources().getColor(R.color.red_SIAPCERIA));
                snackbar.show();
            }
        });
        view.findViewById(R.id.layanandarurat).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar snackbar;
                snackbar = Snackbar.make(view, "Menu ini akan segera hadir", Snackbar.LENGTH_SHORT);
                View snackBarView = snackbar.getView();
                snackBarView.setBackgroundColor(getResources().getColor(R.color.red_SIAPCERIA));
                snackbar.show();
            }
        });
        view.findViewById(R.id.kritiksaran).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                KritikSaranFragment secondFragtry = new KritikSaranFragment();
                FragmentManager fm = getActivity().getSupportFragmentManager();
                fm.beginTransaction().replace(R.id.flMain, secondFragtry).commit();
            }
        });
        view.findViewById(R.id.informasipublik).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PelayananPublik();
            }
        });
        view.findViewById(R.id.pojokaduan).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar snackbar;
                snackbar = Snackbar.make(view, "Menu ini akan segera hadir", Snackbar.LENGTH_SHORT);
                View snackBarView = snackbar.getView();
                snackBarView.setBackgroundColor(getResources().getColor(R.color.red_SIAPCERIA));
                snackbar.show();
            }
        });

        view.findViewById(R.id.pantauAntrian).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar snackbar;
                snackbar = Snackbar.make(view, "Menu ini akan segera hadir", Snackbar.LENGTH_SHORT);
                View snackBarView = snackbar.getView();
                snackBarView.setBackgroundColor(getResources().getColor(R.color.red_SIAPCERIA));
                snackbar.show();
            }
        });

        view.findViewById(R.id.daftarControl).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar snackbar;
                snackbar = Snackbar.make(view, "Menu ini akan segera hadir", Snackbar.LENGTH_SHORT);
                View snackBarView = snackbar.getView();
                snackBarView.setBackgroundColor(getResources().getColor(R.color.red_SIAPCERIA));
                snackbar.show();
            }
        });

        view.findViewById(R.id.profileRS).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProfileRS();
            }
        });
        view.findViewById(R.id.infotempattidur).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar snackbar;
                snackbar = Snackbar.make(view, "Menu ini akan segera hadir", Snackbar.LENGTH_SHORT);
                View snackBarView = snackbar.getView();
                snackBarView.setBackgroundColor(getResources().getColor(R.color.red_SIAPCERIA));
                snackbar.show();
            }
        });
        return view;
    }

    private void showBanner() {

        Call<ResponseService<BannerResponse>> bannerCall = NetworkClient.getInstance().getApiService().getBanner(Base.PREFIX_AUTH + token);
        bannerCall.enqueue(new Callback<ResponseService<BannerResponse>>() {
            @Override
            public void onResponse(Call<ResponseService<BannerResponse>> call, retrofit2.Response<ResponseService<BannerResponse>> response) {
                try {
                    List<BannerResponse> item = response.body().getDatalist();
                    Log.e(Base.DEBUG_TEXT, item.toString());
                    mAdapter = new BannerAdapter(item, getContext());
                    slider.setAdapter(mAdapter);
                } catch (Exception ex) {
                    Log.e(Base.DEBUG_TEXT, ex.toString());
                }
            }

            @Override
            public void onFailure(Call<ResponseService<BannerResponse>> call, Throwable t) {
                Log.e(Base.DEBUG_TEXT, t.toString());
            }
        });
    }


    private void namaa() {
        String url;
        url = url_insert + "?token=" + Preferences.getLoginToken(getContext());
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        StringRequest strReq = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    // get JSONObject from JSON file
                    JSONObject obj = new JSONObject(response);
                    // fetch JSONObject named employee
                    JSONObject employee = obj.getJSONObject("response");
                    String message = employee.getString("email");
                    String firstname = employee.getString("firstname");
                    String lastname = employee.getString("lastname");
                    Log.d("OBJECT", message);
//                    Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
                    // get employee name and salary
                    emaile = employee.getString("email");
                    firstname = employee.getString("firstname");
                    lastname = employee.getString("lastname");
                    nik = employee.getString("nik");
                    alamat = employee.getString("alamat");
                    nohp = employee.getString("nohp");
                    userid = employee.getString("id");
                    email.setText(emaile);
                    nama.setText(firstname + " " + lastname);

                    fetchJSONValidasiBooking();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getActivity(), "error", Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {

                // Posting parameters ke post url
                Map<String, String> params = new HashMap<String, String>();
                // jika id kosong maka simpan, jika id ada nilainya maka update

                params.put("token", indexActivity.getToken());

                return params;
            }
        };

        queue.add(strReq);

    }

    private void fetchJSONValidasiBooking() {
        pDialog = new ProgressDialog(getContext());
        pDialog.setCancelable(false);
        pDialog.setMessage(Base.LOADING_TEXT);
        showDialog();
        // REST LOGIN ------------------------------------------------------------------
        RestServices restServices = ServiceGenerator.build().create(RestServices.class);
        Call getbooking = restServices.ListBooking(userid, Base.PREFIX_AUTH + token);

        getbooking.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, retrofit2.Response response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        hideDialog();
                        String jsonresponse = response.body().toString();
                        Log.i("datas", jsonresponse);
//                        spinJSONValidasiBooking(jsonresponse);
//                        barcode();
//                        ll.setVisibility(View.VISIBLE);
                    } else {
                        hideDialog();
                        ll.setVisibility(View.GONE);
                    }
                } else {
                    hideDialog();
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                hideDialog();
                Log.i("onFailure", t.getMessage().toString());
            }
        });
    }

    private void spinJSONValidasiBooking(String response) {
        try {
            JSONObject obj = new JSONObject(response);
            JSONObject rrrr = obj.getJSONObject("response");
            JSONArray dataArray = rrrr.getJSONArray("booking");
            for (int i = 0; i < dataArray.length(); i++) {

                mBooking modelListView = new mBooking();
                JSONObject dataobj = dataArray.getJSONObject(i);

                txt_booking_nama.setText(dataobj.getString("nama_pasien"));
                txt_booking_jaminan.setText(dataobj.getString("penjamin"));
                txt_booking_dr.setText(dataobj.getString("NAMADOKTER"));
                txt_booking_kode.setText(dataobj.getString("bookingcode"));
//                txttgldaftarhome.setText("Tanggal daftar : " + dataobj.getString("tanggal"));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void Daftar() {
        // TODO Auto-generated method stub
        DaftarFragment secondFragtry = new DaftarFragment();
        FragmentManager fm = getActivity().getSupportFragmentManager();
        fm.beginTransaction().replace(R.id.flMain, secondFragtry).commit();
    }

    private void Jadwal() {
        // TODO Auto-generated method stub
        JadwalFragment secondFragtry = new JadwalFragment();
        FragmentManager fm = getActivity().getSupportFragmentManager();
        fm.beginTransaction().replace(R.id.flMain, secondFragtry).commit();
    }

    private void ProfileRS() {
        // TODO Auto-generated method stub
        ProfileRSFragment secondFragtry = new ProfileRSFragment();
        FragmentManager fm = getActivity().getSupportFragmentManager();
        fm.beginTransaction().replace(R.id.flMain, secondFragtry).commit();
    }
    private void PelayananPublik() {
        // TODO Auto-generated method stub
        PelayananPublikFragment secondFragtry = new PelayananPublikFragment();
        FragmentManager fm = getActivity().getSupportFragmentManager();
        fm.beginTransaction().replace(R.id.flMain, secondFragtry).commit();
    }


    public void barcode() {
        try {
            BitMatrix bitMatrix = multiFormatWriter.encode(txt_booking_kode.getText().toString(), BarcodeFormat.QR_CODE, 300, 300);
            BarcodeEncoder encoder = new BarcodeEncoder();
            Bitmap bitmap = encoder.createBitmap(bitMatrix);
            image.setImageBitmap(bitmap);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

}
