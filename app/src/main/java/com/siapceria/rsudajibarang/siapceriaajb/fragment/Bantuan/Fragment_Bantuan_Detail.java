package com.siapceria.rsudajibarang.siapceriaajb.fragment.Bantuan;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.siapceria.rsudajibarang.siapceriaajb.R;
import com.siapceria.rsudajibarang.siapceriaajb.adapter.BantuanAdapter;
import com.siapceria.rsudajibarang.siapceriaajb.helper.ServiceGenerator;
import com.siapceria.rsudajibarang.siapceriaajb.indexActivity;
import com.siapceria.rsudajibarang.siapceriaajb.model.mBantuan;
import com.siapceria.rsudajibarang.siapceriaajb.service.RestServices;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Fragment_Bantuan_Detail extends Fragment {
    public static String KEY_ID = "id";
    public static String KEY_BANTUAN = "bantuan";
    public static String KEY_PENANGANAN = "penanganan";

    String getid,getbantuan,getpenanganan;
    TextView tvpertanyaan,tvjawaban,tvkosong;
    Button btnwa;
    public static ProgressDialog pDialog;
    private ArrayList<mBantuan> goodModelBantuanArrayList;
    private BantuanAdapter bantuanAdapter;

    RecyclerView listView;

    public Fragment_Bantuan_Detail() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_bantuan_detail, container, false);
        getid = getArguments().getString(KEY_ID);
        getbantuan = getArguments().getString(KEY_BANTUAN);
        getpenanganan = getArguments().getString(KEY_PENANGANAN);
        tvpertanyaan = (TextView)view.findViewById(R.id.tvbulanandetail);
//        tvjawaban = (TextView)view.findViewById(R.id.tvjawabandetail);
        tvkosong = (TextView) view.findViewById(R.id.tvkosong);
        btnwa=(Button)view.findViewById(R.id.btn_wa);
        listView=(RecyclerView)view.findViewById(R.id.listViewbantuan);
        RecyclerView.LayoutManager gridlay;
        gridlay = new GridLayoutManager(getActivity(), 1);
        listView.setLayoutManager(gridlay);

        tvpertanyaan.setText(getbantuan);
//        tvjawaban.setText(getpenanganan);

        Toolbar toolbar = view.findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        toolbar.setTitle("Back");
        toolbar.setTitleTextColor(getResources().getColor(android.R.color.white));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @SuppressLint("WrongConstant")
            @Override
            public void onClick(View v) {
//                Toasty.error(getActivity(), "Pilih Tanggal Control", Toast.LENGTH_LONG).show();
                Fragment_Bantuan secondFragtry = new Fragment_Bantuan();
                FragmentManager fm = getActivity().getSupportFragmentManager();
                fm.beginTransaction().replace(R.id.flMain, secondFragtry).commit();
            }
        });

        btnwa.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                wa();
            }
        });
        getdata();
        return view;
    }

    private void wa(){
        try {
            String headerReceiver = "";// Replace with your message.
            String bodyMessageFormal = "";// Replace with your message.
            String whatsappContain = headerReceiver + bodyMessageFormal;
            String trimToNumner = "+6285640769886"; //10 digit number
            Intent intent = new Intent ( Intent.ACTION_VIEW );
            intent.setData ( Uri.parse ( "https://wa.me/" + trimToNumner + "/?text=" + "" ) );
            startActivity ( intent );
        } catch (Exception e) {
            e.printStackTrace ();
        }
    }

    private void getdata() {
        pDialog = new ProgressDialog(getContext());
        pDialog.setCancelable(false);
        pDialog.setMessage("Loading ...");
        showDialog();
        String id = indexActivity.getIdUser();
        // REST LOGIN ------------------------------------------------------------------
        RestServices restServices = ServiceGenerator.build().create(RestServices.class);
        Call bantuan = restServices.ListBantuanDetail(getid);

        bantuan.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                Log.i("TAGI", "jalannnn");
                try {
                    Log.i("TAGI", response.body().toString());
                    //Toast.makeText()
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            Log.i("onSuccess", response.body().toString());

                            String jsonresponse = response.body().toString();
                            writeListView(jsonresponse);
                            hideDialog();

                        } else {
                            Log.i("onEmptyResponse", "Returned empty response");//Toast.makeText(getContext(),"Nothing returned",Toast.LENGTH_LONG).show();
                            hideDialog();
                        }
                    }
                }
                catch (Exception ex){
                    hideDialog();
                    tvkosong.setVisibility(View.VISIBLE);
                    tvkosong.setText("Tidak ada Jawaban");
                }
            }


            @Override
            public void onFailure(Call call, Throwable t) {
                Log.i("onFailure",t.getMessage().toString());
                hideDialog();
            }
        });
    }

    private void writeListView(String response){

        try {
            //getting the whole json object from the response
            JSONObject obj = new JSONObject(response);
            JSONObject rrrr = obj.getJSONObject("response");

            goodModelBantuanArrayList = new ArrayList<>();
            JSONArray dataArray  = rrrr.getJSONArray("jawaban");
            for (int i = 0; i < dataArray.length(); i++) {

                mBantuan modelListView = new mBantuan();
                JSONObject dataobj = dataArray.getJSONObject(i);

                modelListView.setId(dataobj.getString("id_pertanyaan"));
                modelListView.setBantuan(dataobj.getString("jawaban"));
//                modelListView.setPenanganan(dataobj.getString("penanganan"));

                goodModelBantuanArrayList.add(modelListView);

            }

            bantuanAdapter = new BantuanAdapter(getActivity(), goodModelBantuanArrayList);
            listView.setAdapter(bantuanAdapter);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}
