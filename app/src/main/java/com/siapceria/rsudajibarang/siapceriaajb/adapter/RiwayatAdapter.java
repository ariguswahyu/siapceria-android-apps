package com.siapceria.rsudajibarang.siapceriaajb.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.MultiFormatWriter;
import com.journeyapps.barcodescanner.BarcodeEncoder;
import com.siapceria.rsudajibarang.siapceriaajb.R;
import com.siapceria.rsudajibarang.siapceriaajb.fragment.Riwayat.FragmentRiwayatDetail;
import com.siapceria.rsudajibarang.siapceriaajb.model.mRiwayat;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import es.dmoral.toasty.Toasty;

public class RiwayatAdapter extends RecyclerView.Adapter<RiwayatAdapter.MyViewHolder> {

    private LayoutInflater inflater;
    private LinearLayout head_title;
    private ArrayList<mRiwayat> dataModelArrayList;
    public static String KEY_ID = "id";
    public static String KEY_BOOKING = "bookingcode";
    MultiFormatWriter multiFormatWriter = new MultiFormatWriter();

    private Context context;
    SimpleDateFormat simpleDateFormat;
//    private  Date date;

    public RiwayatAdapter(Context context, ArrayList<mRiwayat> dataModelArrayList) {

        this.context = context;
        inflater = LayoutInflater.from(context);
        this.dataModelArrayList = dataModelArrayList;
//        this.listener = listener;
    }

    @Override
    public RiwayatAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.list_riwayat, parent, false);
        MyViewHolder holder = new MyViewHolder(view);


        return holder;
    }

    @Override
    public void onBindViewHolder(RiwayatAdapter.MyViewHolder holder, int position) {

        holder.tvtanggal.setText(dataModelArrayList.get(position).getTanggal());
        holder.etnamapasienriwayat.setText(dataModelArrayList.get(position).getNomr() + ", " + dataModelArrayList.get(position).getNama_title());
        holder.ettipe_penjaminriwayat.setText(dataModelArrayList.get(position).getPenjamin() + " ( POLI " + dataModelArrayList.get(position).getPoliklinik() + ")");
        holder.etdokterriwayat.setText(dataModelArrayList.get(position).getDokter());
        holder.txtkodebooking.setText(dataModelArrayList.get(position).getKodebooking());
        holder.etNobpjs.setText(dataModelArrayList.get(position).getNobpjs());
        holder.etNoRujukanbpjs.setText(dataModelArrayList.get(position).getNorujukan());



        String str_date = dataModelArrayList.get(position).getEstimasipelayanan();
        DateFormat formatter = new SimpleDateFormat("yyyy-mm-dd HH:mm:ss");
        try {
            Date date = formatter.parse(str_date);
            Log.i("RUPS", date.toString());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (dataModelArrayList.get(position).getNoantrian().equals("null")) {
            holder.noantrian.setText("-");
        } else {
            holder.noantrian.setText(dataModelArrayList.get(position).getNoantrian());
        }

        if (dataModelArrayList.get(position).getPenjamin().equals("UMUM")) {
            holder.label_etNobpjs.setVisibility(View.GONE);
            holder.etNobpjs.setVisibility(View.GONE);
            holder.label_etNoRujukanbpjs.setVisibility(View.GONE);
            holder.etNoRujukanbpjs.setVisibility(View.GONE);
        } else {
            holder.label_etNobpjs.setVisibility(View.VISIBLE);
            holder.etNobpjs.setVisibility(View.VISIBLE);
            holder.label_etNoRujukanbpjs.setVisibility(View.VISIBLE);
            holder.etNoRujukanbpjs.setVisibility(View.VISIBLE);
        }

        if (dataModelArrayList.get(position).getEstimasipelayanan().equals("null")) {
            holder.estimasipelayanan.setText("-");
            holder.tvEstimasi.setText("-");
        } else {
            holder.estimasipelayanan.setText(dataModelArrayList.get(position).getEstimasipelayanan());
            holder.tvEstimasi.setText(dataModelArrayList.get(position).getEstimasipelayanan());
        }

        if (dataModelArrayList.get(position).getIsBoarded().equals("1")) {
            holder.head_title.setBackgroundResource(R.color.abuabu);
            holder.tv_waktu.setText("Sudah Melakukan Boarding");
        } else if (dataModelArrayList.get(position).getIsBoarded().equals("3")) {
            holder.head_title.setBackgroundResource(R.color.red_SIAPCERIA);
            holder.tv_waktu.setText("Pendaftaran dibatalkan");
        } else {
            holder.head_title.setBackgroundResource(R.color.birunyacinta);
            holder.tv_waktu.setText("");
        }

        holder.barcode();
    }

    @Override
    public int getItemCount() {

        return dataModelArrayList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tvtanggal, etnamapasienriwayat, etdokterriwayat, ettipe_penjaminriwayat, tv_waktu, label_etNobpjs, etNobpjs, label_etNoRujukanbpjs, etNoRujukanbpjs, noantrian, estmsilayanan,tvEstimasi, estimasipelayanan;
        ImageView qrcoderiwayat;
        TextView txtkodebooking;
        LinearLayout head_title;

        public MyViewHolder(View itemView) {
            super(itemView);

            tvtanggal = (TextView) itemView.findViewById(R.id.tvtanggallist);
            etnamapasienriwayat = (TextView) itemView.findViewById(R.id.etnamapasienriwayat);
            ettipe_penjaminriwayat = (TextView) itemView.findViewById(R.id.ettipe_penjaminriwayat);
            etdokterriwayat = (TextView) itemView.findViewById(R.id.etdokterriwayat);
            txtkodebooking = (TextView) itemView.findViewById(R.id.txtkodebooking);
            qrcoderiwayat = (ImageView) itemView.findViewById(R.id.qrcoderiwayat);
            head_title = (LinearLayout) itemView.findViewById(R.id.head_title);
            tv_waktu = (TextView) itemView.findViewById(R.id.tv_waktu);

            label_etNobpjs = (TextView) itemView.findViewById(R.id.label_etNobpjs);
            etNobpjs = (TextView) itemView.findViewById(R.id.etNobpjs);
            label_etNoRujukanbpjs = (TextView) itemView.findViewById(R.id.label_etNoRujukanbpjs);
            etNoRujukanbpjs = (TextView) itemView.findViewById(R.id.etNoRujukanbpjs);

            noantrian = (TextView) itemView.findViewById(R.id.tx_noAntrian);
            estimasipelayanan = (TextView) itemView.findViewById(R.id.txt_estimasipelayanan);
            tvEstimasi = (TextView) itemView.findViewById(R.id.tvEstimasi);

            itemView.setClickable(true);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (dataModelArrayList.get(getPosition()).getIsBoarded().equals("1")) {
                Toasty.warning(context, "Anda sudah melakukan Boarding ", Toast.LENGTH_SHORT).show();
            } else if (dataModelArrayList.get(getPosition()).getIsBoarded().equals("3")) {
                Toasty.error(context, "Maaf, Pendaftaran anda telah dibatalkan", Toast.LENGTH_SHORT).show();
            } else {
                FragmentRiwayatDetail secondFragtry = new FragmentRiwayatDetail();
                Bundle mBundle = new Bundle();
                mBundle.putString(KEY_BOOKING, txtkodebooking.getText().toString());
                secondFragtry.setArguments(mBundle);
                FragmentManager fm = ((AppCompatActivity) context).getSupportFragmentManager();
                fm.beginTransaction().replace(R.id.flMain, secondFragtry).commit();
            }

        }

        public void barcode() {
            try {
                BitMatrix bitMatrix = multiFormatWriter.encode(txtkodebooking.getText().toString(), BarcodeFormat.QR_CODE, 300, 300);
                BarcodeEncoder encoder = new BarcodeEncoder();
                Bitmap bitmap = encoder.createBitmap(bitMatrix);
                qrcoderiwayat.setImageBitmap(bitmap);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}