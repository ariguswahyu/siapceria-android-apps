package com.siapceria.rsudajibarang.siapceriaajb.model;

public class mJadwalDetail {
    private String id;
    private String tanggal;
    private String poly;
    private String kddokter;
    private String nama_dokter;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getPoly() {
        return poly;
    }

    public void setPoly(String poly) {
        this.poly = poly;
    }

    public String getNama_dokter() {
        return nama_dokter;
    }

    public void setNama_dokter(String nama_dokter) {
        this.nama_dokter = nama_dokter;
    }

    public String getKddokter() {
        return kddokter;
    }

    public void setKddokter(String kddokter) {
        this.kddokter = kddokter;
    }
}
