package com.siapceria.rsudajibarang.siapceriaajb.model.dokterjaga;

import com.google.gson.annotations.SerializedName;

public class MetaData{

	@SerializedName("code")
	private int code;

	@SerializedName("message")
	private String message;

	public int getCode(){
		return code;
	}

	public String getMessage(){
		return message;
	}
}