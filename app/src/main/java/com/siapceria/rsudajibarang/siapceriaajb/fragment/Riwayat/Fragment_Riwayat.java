package com.siapceria.rsudajibarang.siapceriaajb.fragment.Riwayat;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.siapceria.rsudajibarang.siapceriaajb.R;
import com.siapceria.rsudajibarang.siapceriaajb.adapter.RiwayatAdapter;
import com.siapceria.rsudajibarang.siapceriaajb.constant.Base;
import com.siapceria.rsudajibarang.siapceriaajb.fragment.HomeFragment;
import com.siapceria.rsudajibarang.siapceriaajb.helper.ServiceGenerator;
import com.siapceria.rsudajibarang.siapceriaajb.model.mBooking;
import com.siapceria.rsudajibarang.siapceriaajb.model.mRiwayat;
import com.siapceria.rsudajibarang.siapceriaajb.service.RestServices;
import com.siapceria.rsudajibarang.siapceriaajb.sharedpreferences.login.Preferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.R.layout.simple_spinner_item;

/**
 * A simple {@link Fragment} subclass.
 */
public class Fragment_Riwayat extends Fragment {
    private ArrayList<mRiwayat> goodModelRiwayatArrayList;
    private RiwayatAdapter riwayatAdapter;
    RecyclerView listView;
    String token;
    public static ProgressDialog pDialog;

    private Spinner spfilter;
    private TextView tvfilterriwayat,tvkosong;
    private ArrayList<mBooking> goodModelBookingArrayList;
    List<String> valueId = new ArrayList<String>();
    List<String> valueNama = new ArrayList<String>();

    public Fragment_Riwayat() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_riwayat, container, false);
        listView=(RecyclerView) view.findViewById(R.id.rcvriwayat);
        goodModelRiwayatArrayList = new ArrayList<>();
        riwayatAdapter = new RiwayatAdapter(getActivity(), goodModelRiwayatArrayList);
        listView.setAdapter(riwayatAdapter);

        RecyclerView.LayoutManager gridlay;
        gridlay = new GridLayoutManager(getActivity(), 1);
        listView.setLayoutManager(gridlay);

        token = Preferences.getLoginToken(getActivity().getApplicationContext());

        spfilter = (Spinner) view.findViewById(R.id.spfilterriwayat);
        tvfilterriwayat = (TextView) view.findViewById(R.id.tvfilterriwayat);
        tvkosong = (TextView) view.findViewById(R.id.tvkosong);

        Toolbar toolbar = view.findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        toolbar.setTitle("Back");
        toolbar.setTitleTextColor(getResources().getColor(android.R.color.white));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @SuppressLint("WrongConstant")
            @Override
            public void onClick(View v) {
//                Toasty.error(getActivity(), "Pilih Tanggal Control", Toast.LENGTH_LONG).show();
                HomeFragment secondFragtry = new HomeFragment();
                FragmentManager fm = getActivity().getSupportFragmentManager();
                fm.beginTransaction().replace(R.id.flMain, secondFragtry).commit();
            }
        });



//
        fetchJSONFilter();
//
        spfilter.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                tvfilterriwayat.setText(valueNama.get(position));
                Log.i("onItemSelected",valueId.toString());
                getdata();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
        return view;
    }

    private void getdata() {
        Log.i("CEK_ID", tvfilterriwayat.getText().toString());
        String id =  Preferences.getUserId(getActivity().getApplicationContext());
        Log.i("CEK_ID", "dari shared pref : "+Preferences.getUserId(getActivity().getApplicationContext()));
        // REST LOGIN ------------------------------------------------------------------
        RestServices restServices = ServiceGenerator.build().create(RestServices.class);
        Call riwayat = restServices.ListBookingDetail(id, tvfilterriwayat.getText().toString(), Base.PREFIX_AUTH +token);

        riwayat.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                Log.i("TAGI", "jalannnn");
                try {
                    Log.i("TAGI", response.body().toString());
                    //Toast.makeText()
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            Log.i("datasss", response.body().toString());

                            String jsonresponse = response.body().toString();
                            writeListView(jsonresponse);
                            tvkosong.setVisibility(View.GONE);

                        } else {
                            Log.i("onEmptyResponse", "Returned empty response");//Toast.makeText(getContext(),"Nothing returned",Toast.LENGTH_LONG).show();
                        }
                    }
                }
                catch (Exception ex)
                {
                    tvkosong.setVisibility(View.VISIBLE);
                    tvkosong.setText("Tidak ada Riwayat");
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                Log.i("onFailure",t.getMessage().toString());
            }
        });
    }

    private void writeListView(String response){

        try {
            //getting the whole json object from the response
            JSONObject obj = new JSONObject(response);
            JSONObject rrrr = obj.getJSONObject("response");
            Log.i("writeListView",rrrr.toString());
            goodModelRiwayatArrayList = new ArrayList<>();

            JSONArray dataArray  = rrrr.getJSONArray("bookinglist");

            Log.i("datasss", dataArray.toString());

            for (int i = 0; i < dataArray.length(); i++) {

                mRiwayat modelListView = new mRiwayat();
                JSONObject dataobj = dataArray.getJSONObject(i);

                modelListView.setId(dataobj.getString("id"));
                modelListView.setTanggal(dataobj.getString("tanggal"));
                modelListView.setNomr(dataobj.getString("nama_pasien"));
                modelListView.setPenjamin(dataobj.getString("penjamin"));
                modelListView.setDokter(dataobj.getString("NAMADOKTER"));
                modelListView.setKodebooking(dataobj.getString("bookingcode"));
                modelListView.setIsBoarded(dataobj.getString("isBoarded"));
                modelListView.setPoliklinik(dataobj.getString("poliklinik"));
                modelListView.setNama_title(dataobj.getString("nama_title"));
                modelListView.setNomr(dataobj.getString("nomr"));

                modelListView.setNobpjs(dataobj.getString("nobpjs"));
                modelListView.setNorujukan(dataobj.getString("norujukan"));


                modelListView.setNoantrian(dataobj.getString("noantrian"));
                modelListView.setEstmsilayanan(dataobj.getString("estmsilayanan"));
                modelListView.setEstimasipelayanan(dataobj.getString("estimasipelayanan"));

                goodModelRiwayatArrayList.add(modelListView);

            }


//
            riwayatAdapter = new RiwayatAdapter(getActivity(), goodModelRiwayatArrayList);
            Log.i("writeListView",riwayatAdapter.toString());
            listView.setAdapter(riwayatAdapter);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }



    private void fetchJSONFilter(){
        String id =  Preferences.getUserId(getActivity().getApplicationContext());
        pDialog = new ProgressDialog(getContext());
        pDialog.setCancelable(false);
        pDialog.setMessage("Loading ...");
        showDialog();
        // REST LOGIN ------------------------------------------------------------------
        RestServices restServices = ServiceGenerator.build().create(RestServices.class);
        Call filter = restServices.FilterBooking(id, "Bearer "+token);

        filter.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {

                try {
                    Log.i("Responsestring", response.body().toString());
                    //Toast.makeText()
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            Log.i("onSuccess", response.body().toString());

                            String jsonresponse = response.body().toString();
                            spinJSONFilter(jsonresponse);
                            tvkosong.setVisibility(View.GONE);
                            hideDialog();

                        } else {
                            Log.i("onEmptyResponse", "Returned empty response");//Toast.makeText(getContext(),"Nothing returned",Toast.LENGTH_LONG).show();
                            hideDialog();
                        }
                    }
//                    else {
//                        Toasty.error(getActivity(), "Tidak ada Riwayat");
//                    }
                }
                catch (Exception ex){
                    hideDialog();
                    tvkosong.setVisibility(View.VISIBLE);
                    tvkosong.setText("Tidak ada Riwayat");
//                    Toasty.error(getActivity(), "Tidak ada Riwayat");
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                Log.i("onFailure",t.getMessage().toString());
                hideDialog();
            }
        });
    }

    private void spinJSONFilter(String response){

        try {

            JSONObject obj = new JSONObject(response);
            JSONObject rrrr = obj.getJSONObject("response");

            goodModelBookingArrayList = new ArrayList<>();
            JSONArray dataArray  = rrrr.getJSONArray("bookinglist");

            for (int i = 0; i < dataArray.length(); i++) {

                mBooking spinnerModel = new mBooking();
                JSONObject dataobj = dataArray.getJSONObject(i);

                spinnerModel.setId(dataobj.getString("id"));
                spinnerModel.setNama_pasien(dataobj.getString("nama_pasien"));

                goodModelBookingArrayList.add(spinnerModel);

            }

//
            for (int i = 0; i < goodModelBookingArrayList.size(); i++){
                valueId.add(goodModelBookingArrayList.get(i).getId().toString());
                valueNama.add(goodModelBookingArrayList.get(i).getNama_pasien().toString());
            }

            Log.i("spinJSONFilter",valueId.toString());
            Log.i("spinJSONFilter",valueNama.toString());
//
//
            ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getActivity(), simple_spinner_item, valueNama);
            Log.i("spinJSONFilter",spinnerArrayAdapter.toString());
            spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
            spfilter.setAdapter(spinnerArrayAdapter);
            spinnerArrayAdapter.notifyDataSetChanged();

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

}
