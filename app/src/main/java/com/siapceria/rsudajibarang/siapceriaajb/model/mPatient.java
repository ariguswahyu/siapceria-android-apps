package com.siapceria.rsudajibarang.siapceriaajb.model;

public class mPatient {
    private String id;
    private String NOMR;
    private String TITLE;
    private String NAMA;
    private String IBUKANDUNG;
    private String TEMPAT;
    private String TGLLAHIR;
    private String JENISKELAMIN;
    private String ALAMAT;
    private String KELURAHAN;
    private String KDKECAMATAN;
    private String KOTA;
    private String KDPROVINSI;
    private String NOTELP;
    private String NOKTP;
    private String SUAMI_ORTU;
    private String PEKERJAAN;
    private String STATUS;
    private String AGAMA;
    private String PENDIDIKAN;
    private String NAMAAYAH;
    private String NAMAIBU;
    private String SUAMI;
    private String ISTRI;
    private String SUKU;
    private String BAHASADAERAH;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNOMR() {
        return NOMR;
    }

    public void setNOMR(String NOMR) {
        this.NOMR = NOMR;
    }

    public String getTITLE() {
        return TITLE;
    }

    public void setTITLE(String TITLE) {
        this.TITLE = TITLE;
    }

    public String getNAMA() {
        return NAMA;
    }

    public void setNAMA(String NAMA) {
        this.NAMA = NAMA;
    }

    public String getIBUKANDUNG() {
        return IBUKANDUNG;
    }

    public void setIBUKANDUNG(String IBUKANDUNG) {
        this.IBUKANDUNG = IBUKANDUNG;
    }

    public String getTEMPAT() {
        return TEMPAT;
    }

    public void setTEMPAT(String TEMPAT) {
        this.TEMPAT = TEMPAT;
    }

    public String getTGLLAHIR() {
        return TGLLAHIR;
    }

    public void setTGLLAHIR(String TGLLAHIR) {
        this.TGLLAHIR = TGLLAHIR;
    }

    public String getJENISKELAMIN() {
        return JENISKELAMIN;
    }

    public void setJENISKELAMIN(String JENISKELAMIN) {
        this.JENISKELAMIN = JENISKELAMIN;
    }

    public String getALAMAT() {
        return ALAMAT;
    }

    public void setALAMAT(String ALAMAT) {
        this.ALAMAT = ALAMAT;
    }

    public String getKELURAHAN() {
        return KELURAHAN;
    }

    public void setKELURAHAN(String KELURAHAN) {
        this.KELURAHAN = KELURAHAN;
    }

    public String getKDKECAMATAN() {
        return KDKECAMATAN;
    }

    public void setKDKECAMATAN(String KDKECAMATAN) {
        this.KDKECAMATAN = KDKECAMATAN;
    }

    public String getKOTA() {
        return KOTA;
    }

    public void setKOTA(String KOTA) {
        this.KOTA = KOTA;
    }

    public String getKDPROVINSI() {
        return KDPROVINSI;
    }

    public void setKDPROVINSI(String KDPROVINSI) {
        this.KDPROVINSI = KDPROVINSI;
    }

    public String getNOTELP() {
        return NOTELP;
    }

    public void setNOTELP(String NOTELP) {
        this.NOTELP = NOTELP;
    }

    public String getNOKTP() {
        return NOKTP;
    }

    public void setNOKTP(String NOKTP) {
        this.NOKTP = NOKTP;
    }

    public String getSUAMI_ORTU() {
        return SUAMI_ORTU;
    }

    public void setSUAMI_ORTU(String SUAMI_ORTU) {
        this.SUAMI_ORTU = SUAMI_ORTU;
    }

    public String getPEKERJAAN() {
        return PEKERJAAN;
    }

    public void setPEKERJAAN(String PEKERJAAN) {
        this.PEKERJAAN = PEKERJAAN;
    }

    public String getSTATUS() {
        return STATUS;
    }

    public void setSTATUS(String STATUS) {
        this.STATUS = STATUS;
    }

    public String getAGAMA() {
        return AGAMA;
    }

    public void setAGAMA(String AGAMA) {
        this.AGAMA = AGAMA;
    }

    public String getPENDIDIKAN() {
        return PENDIDIKAN;
    }

    public void setPENDIDIKAN(String PENDIDIKAN) {
        this.PENDIDIKAN = PENDIDIKAN;
    }

    public String getNAMAAYAH() {
        return NAMAAYAH;
    }

    public void setNAMAAYAH(String NAMAAYAH) {
        this.NAMAAYAH = NAMAAYAH;
    }

    public String getNAMAIBU() {
        return NAMAIBU;
    }

    public void setNAMAIBU(String NAMAIBU) {
        this.NAMAIBU = NAMAIBU;
    }

    public String getSUAMI() {
        return SUAMI;
    }

    public void setSUAMI(String SUAMI) {
        this.SUAMI = SUAMI;
    }

    public String getISTRI() {
        return ISTRI;
    }

    public void setISTRI(String ISTRI) {
        this.ISTRI = ISTRI;
    }

    public String getSUKU() {
        return SUKU;
    }

    public void setSUKU(String SUKU) {
        this.SUKU = SUKU;
    }

    public String getBAHASADAERAH() {
        return BAHASADAERAH;
    }

    public void setBAHASADAERAH(String BAHASADAERAH) {
        this.BAHASADAERAH = BAHASADAERAH;
    }
}
