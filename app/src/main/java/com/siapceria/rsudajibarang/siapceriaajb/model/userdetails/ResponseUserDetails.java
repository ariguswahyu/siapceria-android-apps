package com.siapceria.rsudajibarang.siapceriaajb.model.userdetails;

import com.google.gson.annotations.SerializedName;

public class ResponseUserDetails{

	@SerializedName("metaData")
	private MetaData metaData;

	@SerializedName("response")
	private Response response;

	public void setMetaData(MetaData metaData){
		this.metaData = metaData;
	}

	public MetaData getMetaData(){
		return metaData;
	}

	public void setResponse(Response response){
		this.response = response;
	}

	public Response getResponse(){
		return response;
	}

	@Override
 	public String toString(){
		return 
			"ResponseUserDetails{" + 
			"metaData = '" + metaData + '\'' + 
			",response = '" + response + '\'' + 
			"}";
		}
}