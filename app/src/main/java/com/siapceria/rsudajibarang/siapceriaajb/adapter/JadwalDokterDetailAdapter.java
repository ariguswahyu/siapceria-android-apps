package com.siapceria.rsudajibarang.siapceriaajb.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.siapceria.rsudajibarang.siapceriaajb.R;
import com.siapceria.rsudajibarang.siapceriaajb.model.mJadwalDetail;

import java.util.ArrayList;

public class JadwalDokterDetailAdapter extends RecyclerView.Adapter<JadwalDokterDetailAdapter.MyViewHolder> {

private LayoutInflater inflater;
private ArrayList<mJadwalDetail> dataModelArrayList;
public static String KEY_ID = "id";

    private Context context;
    public JadwalDokterDetailAdapter(Context context, ArrayList<mJadwalDetail> dataModelArrayList){

        this.context=context;
        inflater = LayoutInflater.from(context);
        this.dataModelArrayList = dataModelArrayList;
    }

    @Override
    public JadwalDokterDetailAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.list_jadwal_praktek, parent, false);
        MyViewHolder holder = new MyViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(JadwalDokterDetailAdapter.MyViewHolder holder, int position) {

//        holder.bind(dataModelArrayList.get(position), position);
        holder.posisi=position;
        holder.tvjadwaldetailtanggal.setText(dataModelArrayList.get(position).getTanggal());
    }

    @Override
    public int getItemCount() {
        return dataModelArrayList.size();
    }

class MyViewHolder extends RecyclerView.ViewHolder{

    public int posisi;
    TextView tvjadwaldetailtanggal;

    public MyViewHolder(View itemView) {
        super(itemView);

        tvjadwaldetailtanggal = (TextView) itemView.findViewById(R.id.tvjadwaldetailtanggal);
        itemView.setClickable(true);
//            itemView.setOnClickListener(this);
    }
}

}