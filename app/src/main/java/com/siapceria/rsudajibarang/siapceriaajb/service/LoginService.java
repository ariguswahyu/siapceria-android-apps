package com.siapceria.rsudajibarang.siapceriaajb.service;

import com.siapceria.rsudajibarang.siapceriaajb.model.userLogin;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface LoginService {

    @POST("/auth/login")
    Call<userLogin> login(@Body userLogin user);


}
