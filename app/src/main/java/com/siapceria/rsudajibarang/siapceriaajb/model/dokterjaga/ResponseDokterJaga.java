package com.siapceria.rsudajibarang.siapceriaajb.model.dokterjaga;

import com.google.gson.annotations.SerializedName;

public class ResponseDokterJaga{

	@SerializedName("metaData")
	private MetaData metaData;

	@SerializedName("response")
	private Response response;

	public MetaData getMetaData(){
		return metaData;
	}

	public Response getResponse(){
		return response;
	}
}