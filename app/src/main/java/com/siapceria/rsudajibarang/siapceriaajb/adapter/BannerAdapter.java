package com.siapceria.rsudajibarang.siapceriaajb.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.siapceria.rsudajibarang.siapceriaajb.R;
import com.siapceria.rsudajibarang.siapceriaajb.model.banner.BannerResponse;
import com.squareup.picasso.Picasso;

import java.util.List;

public class BannerAdapter extends PagerAdapter {


    private Context mContext;
    private List<BannerResponse> mItems;
    private LayoutInflater layoutInflater;

    public BannerAdapter(List<BannerResponse> mItems,Context mContext) {
        this.mContext = mContext;
        this.mItems = mItems;
    }

    @Override
    public int getCount() {
        if (mItems != null) {
            return Math.min(mItems.size(), 5);
        } else {
            return 0;
        }

    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
//        super.destroyItem(container, position, object);
        container.removeView((View)object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
//        return super.instantiateItem(container, position);
        layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        final View views = layoutInflater.inflate(R.layout.item_banner,null);
        BannerResponse model = mItems.get(position);
//        TextView txt1 = (TextView) views.findViewById(R.id.titleBanner);
//        TextView txt2 = (TextView) views.findViewById(R.id.desBanner);
        ImageView img = (ImageView) views.findViewById(R.id.imageBanner);

//        txt1.setText(model.getJudul());
//        txt2.setText(model.getKeterangan());
        Picasso.with(mContext)
                .load(model.getGambar())
                .into(img);

        views.setTag(model);
        container.addView(views);
        return views;
    }
}
