package com.siapceria.rsudajibarang.siapceriaajb;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Intent;
import android.content.IntentSender;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.util.Log;

import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.play.core.appupdate.AppUpdateInfo;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.install.model.AppUpdateType;
import com.google.android.play.core.install.model.UpdateAvailability;
import com.google.android.play.core.tasks.OnSuccessListener;
import com.google.android.play.core.tasks.Task;
import com.google.gson.Gson;
import com.orhanobut.hawk.Hawk;
import com.orhanobut.hawk.LogInterceptor;
import com.siapceria.rsudajibarang.siapceriaajb.constant.Base;
import com.siapceria.rsudajibarang.siapceriaajb.helper.ServiceGenerator;
import com.siapceria.rsudajibarang.siapceriaajb.model.user.LoginResponseRepos;
import com.siapceria.rsudajibarang.siapceriaajb.model.userdetails.ResponseUserDetails;
import com.siapceria.rsudajibarang.siapceriaajb.network.NetworkClient;
import com.siapceria.rsudajibarang.siapceriaajb.service.LocalService;
import com.siapceria.rsudajibarang.siapceriaajb.service.NetworkServices;
import com.siapceria.rsudajibarang.siapceriaajb.service.RestServices;
import com.siapceria.rsudajibarang.siapceriaajb.sharedpreferences.login.Preferences;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import es.dmoral.toasty.Toasty;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.Cache;
import okhttp3.CacheControl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;


public class SplashScreen extends AppCompatActivity {


    final static int INTERVAL = 1000;
    final static int TIMEOUT = 2000;
    private RecyclerView recyclerView ;
    ImageView GambarGif;
    RestServices restServices = ServiceGenerator.build().create(RestServices.class);
    private CountDownTimer mCountDownTimer;
    private ProgressBar mProgress;

    private static final int REQ_CODE_VERSION_UPDATE =  101;
    private static final String TAG = "MainActivity";
    private ShimmerFrameLayout mShimmerViewContainer;
    private SwipeRefreshLayout swipeContainer;
    NetworkServices networkServices;
    CompositeDisposable compositeDisposable=new CompositeDisposable();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppUpdateManager appUpdateManager = AppUpdateManagerFactory.create(SplashScreen.this);
        Task<AppUpdateInfo> appUpdateInfoTask = appUpdateManager.getAppUpdateInfo();
        appUpdateInfoTask.addOnSuccessListener(new OnSuccessListener<AppUpdateInfo>() {
            @Override
            public void onSuccess(AppUpdateInfo result) {
                if (result.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE && result.isUpdateTypeAllowed(AppUpdateType.IMMEDIATE)) {
                    try {
                        appUpdateManager.startUpdateFlowForResult(result, AppUpdateType.IMMEDIATE, SplashScreen.this, REQ_CODE_VERSION_UPDATE);
                    } catch (IntentSender.SendIntentException e) {
//                        e.printStackTrace();
                        Toasty.error(getApplicationContext(), "Gagal Update", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

//        setupView();
//        setupRetroOkHttp();
//        fetchData();

//        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
//            @Override
//            public void onRefresh() {
//
////                fetchData();
//            }
//        });

    }

    private void fetchData() {
        Log.i("JAVARX", "start");
        String userId = Preferences.getUserId(getBaseContext());
        Log.i("JAVARX", userId.toString());
        String token = Preferences.getLoginToken(getBaseContext());
        Log.i("JAVARX", token.toString());


        networkServices.AccountDetails(userId,Base.PREFIX_AUTH+token)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ResponseUserDetails>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
//                        Log.i("JAVARX", d.toString());
                    }

                    @Override
                    public void onNext(@NonNull ResponseUserDetails responseUserDetails) {
                        displayData(responseUserDetails);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
//                        Log.i("JAVARX", e.getMessage().toString());
                    }

                    @Override
                    public void onComplete() {
//                        Log.i("JAVARX", "complete");
                    }
                });



    }

    private void displayData(ResponseUserDetails responseUserDetails) {

        Log.i("JAVARX", "responseUserDetails");
        Log.i("JAVARX", responseUserDetails.toString());
    }

    private void setupView() {

        swipeContainer = findViewById(R.id.swipeContainer);
        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);
//        menu= findViewById(R.id.menu);
        recyclerView = findViewById(R.id.recycler);
//        recyclerView.hasFixedSize();
//        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    private Interceptor provideCacheInterceptor() {

        return new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request request = chain.request();
                okhttp3.Response originalResponse = chain.proceed(request);
                String cacheControl = originalResponse.header("Cache-Control");

                if (cacheControl == null || cacheControl.contains("no-store") || cacheControl.contains("no-cache") ||
                        cacheControl.contains("must-revalidate") || cacheControl.contains("max-stale=0")) {


                    CacheControl cc = new CacheControl.Builder()
                            .maxStale(2, TimeUnit.HOURS)
                            .build();



                    request = request.newBuilder()
                            .cacheControl(cc)
                            .build();

                    return chain.proceed(request);

                } else {
                    return originalResponse;
                }
            }
        };

    }

    private Interceptor provideOfflineCacheInterceptor() {

        return new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                try {
                    return chain.proceed(chain.request());
                } catch (Exception e) {


                    CacheControl cacheControl = new CacheControl.Builder()
                            .onlyIfCached()
                            .maxStale(2, TimeUnit.HOURS)
                            .build();

                    Request offlineRequest = chain.request().newBuilder()
                            .cacheControl(cacheControl)
                            .build();
                    return chain.proceed(offlineRequest);
                }
            }
        };
    }

    private void setupRetroOkHttp() {

        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        File httpCacheDirectory = new File(getCacheDir(), "offlineCache");

        //10 MB
        Cache cache = new Cache(httpCacheDirectory, 10 * 1024 * 1024);

        OkHttpClient httpClient = new OkHttpClient.Builder()
                .cache(cache)
                .addInterceptor(httpLoggingInterceptor)
                .addNetworkInterceptor(provideCacheInterceptor())
                .addInterceptor(provideOfflineCacheInterceptor())
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(new Gson()))
                .client(httpClient)
                .baseUrl(Base.REST_BASE_URL)
                .build();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQ_CODE_VERSION_UPDATE) {
            Toasty.success(getApplicationContext(), "Memulai Update", Toast.LENGTH_LONG).show();
            if(resultCode!= RESULT_OK){
                Toasty.error(getApplicationContext(), "Update Gagal : "+ requestCode, Toast.LENGTH_LONG).show();
            }
        }
    }




    @Override
    protected void onStart() {
        super.onStart();
        if (Preferences.getLoggedInStatus(getBaseContext())) {
            checkUser();

        } else {
            goToLogin();
        }
    }


    private void goToLogin() {
        setContentView(R.layout.activity_splash_screen);
        //checkStartup();
        mProgress = (ProgressBar) findViewById(R.id.splash_screen_progress_bar);
        // Start lengthy operation in a background thread
        new Thread(new Runnable() {
            public void run() {
                doWork();
                startApp();
                finish();
            }
        }).start();
    }

    private void doWork() {
        for (int progress = 0; progress < 100; progress += 10) {
            try {
                Thread.sleep(500);
                mProgress.setProgress(progress);

            } catch (Exception e) {
                e.printStackTrace();
//                toas.e(e.getMessage());
            }
        }
    }

    private void startApp() {
        Intent intent = new Intent(SplashScreen.this, Login.class);
        startActivity(intent);
    }


    private void initHawk() {
        Hawk.init(this).setLogInterceptor(new LogInterceptor() {
            @Override
            public void onLog(String message) {
                Log.d("Hawk", message);
            }
        }).build();

    }

    private void checkLogin() {

        LoginResponseRepos userlogin = LocalService.getLogin();
        if (userlogin != null) {
            Log.d("checkLogin", "!= null");
            finish();
        } else {
            Log.d("checkLogin", "null");
            finish();
        }

    }

    private void checkStartup() {
        mCountDownTimer = new CountDownTimer(TIMEOUT, INTERVAL) {
            @Override
            public void onTick(long millisUntilFinished) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                    }
                }, 500);
            }

            @Override
            public void onFinish() {
                initHawk();
                checkLogin();
            }
        }.start();
    }


    private void checkUser() {

//        fetchData();
        String userId = Preferences.getUserId(getBaseContext());
        String token = Preferences.getLoginToken(getBaseContext());

        Call<ResponseUserDetails> decodeToken = NetworkClient.getInstance().getApiService().Account(userId, Base.PREFIX_AUTH + token);
        decodeToken.enqueue(new Callback<ResponseUserDetails>() {
            @Override
            public void onResponse(Call<ResponseUserDetails> call, Response<ResponseUserDetails> response) {
                if (response.isSuccessful()) {
                    if (response.code() == 200) {
                        Preferences.setLoggedInStatus(getApplicationContext(), true);
                        String NamaUser = response.body().getResponse().getUser().getFirstname() + " " + response.body().getResponse().getUser().getLastname();
                        Preferences.setLoggedInUser(getApplicationContext(), NamaUser);
                        Preferences.setNikLoggedInUser(getApplicationContext(), response.body().getResponse().getUser().getNik());
                        Preferences.setEmailLoggedInUser(getApplicationContext(), response.body().getResponse().getUser().getEmail());
                        Preferences.setNOHPUser(getApplicationContext(), response.body().getResponse().getUser().getNohp());

                        Intent intent = new Intent(SplashScreen.this, indexActivity.class);
                        finish();
                        startActivity(intent);
                    } else {
                        Preferences.setLoggedInStatus(getApplicationContext(), false);
                        goToLogin();
                    }

                } else {
                    Log.d("checkLogin", "not success");
                    Preferences.setLoggedInStatus(getApplicationContext(), false);
                    goToLogin();
                }
            }

            @Override
            public void onFailure(Call<ResponseUserDetails> call, Throwable t) {
                Log.d("checkLogin", "onFailure");
                Log.d("checkLogin", t.getMessage());
                Preferences.setLoggedInStatus(getApplicationContext(), false);
                goToLogin();
            }
        });
    }


}
