package com.siapceria.rsudajibarang.siapceriaajb.fragment.Bantuan;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.siapceria.rsudajibarang.siapceriaajb.BuildConfig;
import com.siapceria.rsudajibarang.siapceriaajb.R;
import com.siapceria.rsudajibarang.siapceriaajb.adapter.BantuanAdapter;
import com.siapceria.rsudajibarang.siapceriaajb.constant.Base;
import com.siapceria.rsudajibarang.siapceriaajb.fragment.HomeFragment;
import com.siapceria.rsudajibarang.siapceriaajb.helper.ServiceGenerator;
import com.siapceria.rsudajibarang.siapceriaajb.model.mBantuan;
import com.siapceria.rsudajibarang.siapceriaajb.service.RestServices;
import com.siapceria.rsudajibarang.siapceriaajb.sharedpreferences.login.Preferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class Fragment_Bantuan extends Fragment {

    private ArrayList<mBantuan> goodModelBantuanArrayList;
    private BantuanAdapter bantuanAdapter;
    Button btnwa;
    public static ProgressDialog pDialog;
    String token;

    String nowa;

    String versionApp = BuildConfig.VERSION_NAME;
    TextView textView;
    RestServices restServices;

    public Fragment_Bantuan() {
        // Required empty public constructor
    }


    @SuppressLint("WrongViewCast")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_bantuan, container, false);

        token = Preferences.getLoginToken(getContext());
        btnwa = (Button) view.findViewById(R.id.btn_wa);
        setToolbar(view);
        btnwa.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                fetchJSONBantuanHp();
            }
        });


        textView = (TextView) view.findViewById(R.id.label_version_App);
        textView.setText("Siap Ceria Versi : " + versionApp);
        return view;
    }

    private void wa() {
//        fetchJSONBantuanHp();
        try {
            String headerReceiver = "";// Replace with your message.
            String bodyMessageFormal = "";// Replace with your message.
            String whatsappContain = headerReceiver + bodyMessageFormal;
            String trimToNumner = nowa; //10 digit number
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse("https://wa.me/" + trimToNumner + "/?text=" + ""));
            startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setToolbar(View view) {
        Toolbar toolbar = view.findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        toolbar.setTitle(Base.BACK_TEXT);
        toolbar.setTitleTextColor(getResources().getColor(android.R.color.white));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @SuppressLint("WrongConstant")
            @Override
            public void onClick(View v) {
                HomeFragment secondFragtry = new HomeFragment();
                FragmentManager fm = getActivity().getSupportFragmentManager();
                fm.beginTransaction().replace(R.id.flMain, secondFragtry).commit();
            }
        });
    }

    private void LoadingStart() {
        pDialog = new ProgressDialog(getContext());
        pDialog.setCancelable(false);
        pDialog.setMessage(Base.LOADING_TEXT);
        showDialog();
    }

    private void fetchJSONBantuanHp() {
        LoadingStart();
        restServices = ServiceGenerator.build().create(RestServices.class);
        Call getpatient = restServices.BantuanHp(Base.PREFIX_AUTH + token);

        getpatient.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, retrofit2.Response response) {
                if (response.isSuccessful()) {
                    if (response.code() == 200) {
                        if (response.body() != null) {
                            hideDialog();
                            Log.i("onSuccess", response.body().toString());
                            String jsonresponse = response.body().toString();
                            spinJSONBantuanHp(jsonresponse);
                        } else {
                            Log.i("onEmptyResponse", "Returned empty response");//Toast.makeText(getContext(),"Nothing returned",Toast.LENGTH_LONG).show();
                        }
                        hideDialog();
                    }
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                Log.i("onFailure", t.getMessage().toString());
                hideDialog();
            }
        });
    }

    private void spinJSONBantuanHp(String response) {

        try {

            JSONObject obj = new JSONObject(response);
            JSONObject rrrr = obj.getJSONObject("response");
            JSONObject dataArray = rrrr.getJSONObject("bantuan");
            nowa = dataArray.getString("no_hp");

            wa();

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

}
