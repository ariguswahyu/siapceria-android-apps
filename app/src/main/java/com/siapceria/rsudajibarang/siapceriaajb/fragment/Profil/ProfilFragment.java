package com.siapceria.rsudajibarang.siapceriaajb.fragment.Profil;


import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.siapceria.rsudajibarang.siapceriaajb.BuildConfig;
import com.siapceria.rsudajibarang.siapceriaajb.R;
import com.siapceria.rsudajibarang.siapceriaajb.constant.Base;
import com.siapceria.rsudajibarang.siapceriaajb.fragment.HomeFragment;
import com.siapceria.rsudajibarang.siapceriaajb.helper.ServiceGenerator;
import com.siapceria.rsudajibarang.siapceriaajb.service.RestServices;
import com.siapceria.rsudajibarang.siapceriaajb.sharedpreferences.login.Preferences;

import org.json.JSONException;
import org.json.JSONObject;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfilFragment extends Fragment {

    public static String KEY_EMAIL = "email";
    public static String KEY_NAMA = "nama";
    public static String KEY_NIK = "nik";
    public static String KEY_ALAMAT = "alamat";
    public static String KEY_NOHP = "nohp";


    String teksbarcode,email, namaa,nika,alamata,nohpe,token;
    private String url_insert = Base.URL + "auth/decode";
    LinearLayout llprofilpasienterdaftar,llprofilsyaratketentuan;
    TextView nama, lastnamee, emaill,nik, alamat, nohp;
    public static ProgressDialog pDialog;

    String versionApp = BuildConfig.VERSION_NAME;
    TextView textView ;
    public ProfilFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profil, container, false);
        nama = (TextView) view.findViewById(R.id.tvprofilnama);
        nik = (TextView) view.findViewById(R.id.tvprofilnik);
        alamat = (TextView) view.findViewById(R.id.tvprofilalamat);
        nohp = (TextView) view.findViewById(R.id.tvprofilnohp);
        emaill = (TextView) view.findViewById(R.id.txtEmail);
//        llprofilpasienterdaftar = (LinearLayout) view.findViewById(R.id.llprofilpasienterdaftar);

//        token = indexActivity.getToken();//
        token = Preferences.getLoginToken(getActivity().getApplicationContext());


        textView  = (TextView) view.findViewById(R.id.label_version_App);
        textView.setText("Versi : "+versionApp);



        Toolbar toolbar = view.findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        toolbar.setTitle("Back");
        toolbar.setTitleTextColor(getResources().getColor(android.R.color.white));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @SuppressLint("WrongConstant")
            @Override
            public void onClick(View v) {
//                Toasty.error(getActivity(), "Pilih Tanggal Control", Toast.LENGTH_LONG).show();
                HomeFragment secondFragtry = new HomeFragment();
                FragmentManager fm = getActivity().getSupportFragmentManager();
                fm.beginTransaction().replace(R.id.flMain, secondFragtry).commit();
            }
        });
        fetchJSONAccount();
        view.findViewById(R.id.lllogout).setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                Logout();
            }
        });

        view.findViewById(R.id.btnprofiledit).setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                Edit();
            }
        });

//        view.findViewById(R.id.llprofilpasienterdaftar).setOnClickListener(new View.OnClickListener(){
//            public void onClick(View v){
//                Pasien();
//            }
//        });
//
//        view.findViewById(R.id.llprofilsyaratketentuan).setOnClickListener(new View.OnClickListener(){
//            public void onClick(View v){
//                sdank();
//            }
//        });
        return view;

    }

    private void fetchJSONAccount(){
        pDialog = new ProgressDialog(getContext());
        pDialog.setCancelable(false);
        pDialog.setMessage("Loading ...");
        showDialog();
        // REST LOGIN ------------------------------------------------------------------
        RestServices restServices = ServiceGenerator.build().create(RestServices.class);

        String id =  Preferences.getUserId(getActivity().getApplicationContext());
        Call getpatient = restServices.Account(id,"Bearer "+token);

        getpatient.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, retrofit2.Response response) {
//                Log.i("Responsestring", response.body().toString());
                //Toast.makeText()
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        hideDialog();
                        Log.i("onSuccess", response.body().toString());

                        String jsonresponse = response.body().toString();
                        spinJSONAccount(jsonresponse);
                        hideDialog();

                    } else {
                        hideDialog();
                        Log.i("onEmptyResponse", "Returned empty response");//Toast.makeText(getContext(),"Nothing returned",Toast.LENGTH_LONG).show();
                        Toasty.error(getActivity(), "Gagal load data", Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                hideDialog();
                Log.i("onFailure",t.getMessage().toString());
                Toasty.error(getActivity(), "Pasien belum terdaftar", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void spinJSONAccount(String response){

        try {

            JSONObject obj = new JSONObject(response);
            JSONObject rrrr = obj.getJSONObject("response");
            JSONObject dataArray  = rrrr.getJSONObject("user");
            nik.setText(dataArray.getString("nik"));
            nama.setText(dataArray.getString("firstname")+"  "+dataArray.getString("lastname"));
//            tvtemptempatlahir.setText(dataArray.getString("email"));
            alamat.setText(dataArray.getString("alamat"));
            nohp.setText(dataArray.getString("nohp"));
            emaill.setText(dataArray.getString("email"));

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    private void Logout(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                getContext());

        // set title dialog
        alertDialogBuilder.setTitle("Log Out?");

        // set pesan dari dialog
        alertDialogBuilder
                .setMessage("Apakah anda yakin ingin keluar?")
                .setCancelable(false)
                .setPositiveButton("Ya",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        // jika tombol diklik, maka akan menutup activity ini
                        Preferences.setLoggedInStatus(getActivity().getApplicationContext(), false);
                        getActivity().finish();
                    }
                })
                .setNegativeButton("Batalkan",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        // membuat alert dialog dari builder
        AlertDialog alertDialog = alertDialogBuilder.create();

        // menampilkan alert dialog
        alertDialog.show();
    }

    private void Edit() {
        // TODO Auto-generated method stub
        ProfilEditFragment secondFragtry = new ProfilEditFragment();
        Bundle mBundle = new Bundle();
        mBundle.putString(KEY_NIK, nik.getText().toString());
        mBundle.putString(KEY_NAMA, nama.getText().toString());
        mBundle.putString(KEY_ALAMAT, alamat.getText().toString());
        mBundle.putString(KEY_NOHP, nohp.getText().toString());

        secondFragtry.setArguments(mBundle);
        FragmentManager fm = getActivity().getSupportFragmentManager();
        fm.beginTransaction().replace(R.id.flMain, secondFragtry).commit();
    }

    private void Pasien() {
        // TODO Auto-generated method stub

//        ProfilEditFragment secondFragtry = new ProfilEditFragment();
//        FragmentManager fm = getActivity().getSupportFragmentManager();
//        fm.beginTransaction().replace(R.id.flMain, secondFragtry).commit();

        DataPasienFragment secondFragtry = new DataPasienFragment();
//        Bundle mBundle = new Bundle();
//        mBundle.putString(KEY_NIK, nik.getText().toString());
//        mBundle.putString(KEY_NAMA, nama.getText().toString());
//        mBundle.putString(KEY_ALAMAT, alamat.getText().toString());
//        mBundle.putString(KEY_NOHP, nohp.getText().toString());
//
//        secondFragtry.setArguments(mBundle);
        FragmentManager fm = getActivity().getSupportFragmentManager();
        fm.beginTransaction().replace(R.id.flMain, secondFragtry).commit();
    }

    private void sdank() {
        // TODO Auto-generated method stub

//        ProfilEditFragment secondFragtry = new ProfilEditFragment();
//        FragmentManager fm = getActivity().getSupportFragmentManager();
//        fm.beginTransaction().replace(R.id.flMain, secondFragtry).commit();

        DataPasienFragment secondFragtry = new DataPasienFragment();
//        Bundle mBundle = new Bundle();
//        mBundle.putString(KEY_NIK, nik.getText().toString());
//        mBundle.putString(KEY_NAMA, nama.getText().toString());
//        mBundle.putString(KEY_ALAMAT, alamat.getText().toString());
//        mBundle.putString(KEY_NOHP, nohp.getText().toString());
//
//        secondFragtry.setArguments(mBundle);
        FragmentManager fm = getActivity().getSupportFragmentManager();
        fm.beginTransaction().replace(R.id.flMain, secondFragtry).commit();
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

}
