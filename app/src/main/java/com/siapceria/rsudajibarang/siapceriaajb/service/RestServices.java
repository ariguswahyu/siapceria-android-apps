package com.siapceria.rsudajibarang.siapceriaajb.service;

import com.google.gson.JsonObject;
import com.siapceria.rsudajibarang.siapceriaajb.model.dokterjaga.ResponseDokterJaga;
import com.siapceria.rsudajibarang.siapceriaajb.model.forgetpassword.FotgetpasswordResponse;
import com.siapceria.rsudajibarang.siapceriaajb.model.registration.RegistrationResponseRepos;
import com.siapceria.rsudajibarang.siapceriaajb.model.user.LoginResponseRepos;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface RestServices {

    @GET("/api-simrs-rsudajibarang/api/main/users")
    public Call<JsonObject> listUsers();

    @POST("/api-simrs-rsudajibarang/api/auth/login")
    @FormUrlEncoded
    public Call<LoginResponseRepos> Login(@Field("username") String username,
                                          @Field("password") String password);


    @POST("/api-simrs-rsudajibarang/api/auth/forgotpassword")
    @FormUrlEncoded
    public Call<FotgetpasswordResponse> ForgetPassword(@Field("email") String email);


    @GET("/api-simrs-rsudajibarang/api/dokter/dokterjaga/{kodePoli}")
    public Call<JsonObject> ListDokter(@Path("kodePoli") String kodePoli,
                                       @Header("Authorization") String auth);


    @GET("/api-simrs-rsudajibarang/api/dokter/dokterjaga/{kodePoli}/{tanggal}")
    public Call<ResponseDokterJaga> ListDokterPoliklinik(@Path("kodePoli") String kodePoli,
                                                         @Path("tanggal") String tanggal,
                                                         @Header("Authorization") String auth);
    @GET("/api-simrs-rsudajibarang/api/dokter/dokterjaga/{kodePoli}/{tanggal}")
    public Call<JsonObject> ListDokterPoliklinik2(@Path("kodePoli") String kodePoli,
                                                         @Path("tanggal") String tanggal,
                                                         @Header("Authorization") String auth);


    @GET("/api-simrs-rsudajibarang/api/dokter/pilihdokterjaga/{kodePoli}/{tanggal}")
    public Call<JsonObject> ListPilihDokter(@Path("kodePoli") String kodePoli,
                                            @Path("tanggal") String tanggal,
                                            @Header("Authorization") String auth);

    @GET("/api-simrs-rsudajibarang/api/dokter/jadwaldokter/{kdDokter}")
    public Call<JsonObject> ListJadwalDokter(@Path("kdDokter") String kodedokter,
                                             @Header("Authorization") String auth);

    @GET("/api-simrs-rsudajibarang/api/references/bantuan")
    public Call<JsonObject> ListBantuan();

    @GET("/api-simrs-rsudajibarang/api/references/bantuandetail")
    public Call<JsonObject> ListBantuanDetail(@Query("id") String id);

    @POST("/api-simrs-rsudajibarang/api/auth/registration")
    @FormUrlEncoded
    public Call<JsonObject> RegistrationNewUser(@Field("firstname") String firstname,
                                                @Field("lastname") String lastname,
                                                @Field("email") String email,
                                                @Field("password") String password);

    @POST("/api-simrs-rsudajibarang/api/auth/registration")
    @FormUrlEncoded
    public Call<RegistrationResponseRepos> RegistrationUser(
            @Field("firstname") String firstname,
            @Field("lastname") String lastname,
            @Field("email") String email,
            @Field("password") String password);

    @POST("/api-simrs-rsudajibarang/api/auth/updateProfil")
    @FormUrlEncoded
    public Call<RegistrationResponseRepos> UpdateUser(
            @Field("id") String id,
            @Field("nohp") String nohp,
            @Field("alamat") String alamat);


    @POST("/api-simrs-rsudajibarang/api/registration/outPatient")
    @FormUrlEncoded
    public Call<JsonObject> PendaftaranPasienLama(
            @Field("pasienbaru") String pasienbaru,
            @Field("nomr") String nomr,
            @Field("tanggal") String tanggal,
            @Field("poliklinik") String poliklinik,
            @Field("dokter") String dokter,
            @Field("hubungan") String hubungan,
            @Field("notelp") String notelp,
            @Field("email") String email,
            @Field("penjamin") String penjamin,
            @Field("nobpjs") String nobpjs,
            @Field("norujukan") String norujukan,
            @Field("userid") String userid,
            @Header("Authorization") String auth);

    @POST("/api-simrs-rsudajibarang/api/registration/outPatient")
    @FormUrlEncoded
    public Call<JsonObject> PendaftaranPasienBaru(
            @Field("pasienbaru") String pasienbaru,
            @Field("nomr") String nomr,
            @Field("tanggal") String tanggal,
            @Field("poliklinik") String poliklinik,
            @Field("dokter") String dokter,
            @Field("hubungan") String hubungan,
            @Field("tgl_lahir") String tgl_lahir,
            @Field("notelp") String notelp,
            @Field("penjamin") String penjamin,
            @Field("nobpjs") String nobpjs,
            @Field("norujukan") String norujukan,
            @Field("userid") String userid,
            @Field("nama") String nama,
            @Field("nik") String nik,
            @Field("jenis_kelamin") String jenis_kelamin,
            @Field("tempat_lahir") String tempat_lahir,
            @Field("alamat_sesuai_ktp") String alamat_sesuai_ktp,
            @Field("provinsi_id") String provinsi_id,
            @Field("kabupaten_id") String kabupaten_id,
            @Field("kecamatan_id") String kecamatan_id,
            @Field("kelurahan_id") String kelurahan_id,
            @Field("nama_ayah") String nama_ayah,
            @Field("nama_ibu") String nama_ibu,
            @Field("nama_suami") String nama_suami,
            @Field("nama_istri") String nama_istri,
            @Field("agama_id") String agama_id,
            @Field("pendidikan_id") String pendidikan_id,
            @Field("pekerjaan_id") String pekerjaan_id,
            @Field("status_kawin_id") String status_kawin_id,
            @Field("kewarganegaraan") String kewarganegaraan,
            @Field("suku") String suku,
            @Field("bahasa_daerah") String bahasa_daerah,
            @Field("nama_title") String nama_title,
            @Header("Authorization") String auth);

    @GET("/api-simrs-rsudajibarang/api/registration/outPatientList")
    public Call<JsonObject> ListRiwayat(@Query("id") String id,
                                        @Header("Authorization") String auth);

    @GET("/api-simrs-rsudajibarang/api/registration/outPatientList")
    public Call<JsonObject> Decode(@Header("Authorization") String auth);

    @POST("/api-simrs-rsudajibarang/api/registration/outPatient")
    @FormUrlEncoded
    public Call<JsonObject> EditProfil(
            @Field("firstname") String firstname,
            @Field("lastname") String lastname,
            @Field("nik") String nik,
            @Field("alamat") String alamat,
            @Field("nohp") String nohp,
            @Field("email") String email,
            @Field("password") String password,
            @Field("userid") String userid,
            @Header("Authorization") String auth);


    @GET("/api-simrs-rsudajibarang/api/registration/validateNIK")
    public Call<JsonObject> CheckNik(
            @Query("nik") String nik,
            @Header("Authorization") String auth);

    @POST("/api-simrs-rsudajibarang/api/patient/detailsPatient")
    @FormUrlEncoded
    public Call<JsonObject> detailPasien(@Field("nomr") String nomr,
                                         @Field("tgllahir") String tgllahir,
                                         @Header("Authorization") String auth);

    @POST("/api-simrs-rsudajibarang/api/booking/booking")
    @FormUrlEncoded
    public Call<JsonObject> ListBooking(@Field("id") String id,
                                        @Header("Authorization") String auth);

    @POST("/api-simrs-rsudajibarang/api/booking/bookingList")
    @FormUrlEncoded
    public Call<JsonObject> ListBookingDetail(@Field("id") String id,
                                              @Field("nama_pasien") String nama_pasien,
                                              @Header("Authorization") String auth);

    @POST("/api-simrs-rsudajibarang/api/booking/bookingFilter")
    @FormUrlEncoded
    public Call<JsonObject> FilterBooking(@Field("id") String id,
                                          @Header("Authorization") String auth);

    @GET("/api-simrs-rsudajibarang/api/account/detailById/{id}")
    public Call<JsonObject> Account(@Path("id") String id,
                                    @Header("Authorization") String auth);


    // REFERENCES
    @GET("/api-simrs-rsudajibarang/api/references/bantuanTelp")
    public Call<JsonObject> BantuanHp(@Header("Authorization") String auth);

    @GET("/api-simrs-rsudajibarang/api/references/provinsi")
    public Call<JsonObject> ListProvinsi(@Header("Authorization") String auth);

    @GET("/api-simrs-rsudajibarang/api/references/kabupaten")
    public Call<JsonObject> ListKabupaten(@Query("id") String id, @Header("Authorization") String auth);

    @GET("/api-simrs-rsudajibarang/api/references/kecamatan")
    public Call<JsonObject> ListKecamatan(@Query("id") String id, @Header("Authorization") String auth);

    @GET("/api-simrs-rsudajibarang/api/references/kelurahan")
    public Call<JsonObject> ListKelurahan(@Query("id") String id, @Header("Authorization") String auth);

    @GET("/api-simrs-rsudajibarang/api/references/agama")
    public Call<JsonObject> ListAgama(@Header("Authorization") String auth);

    @GET("/api-simrs-rsudajibarang/api/references/pendidikan")
    public Call<JsonObject> ListPendidikan(@Header("Authorization") String auth);

    @GET("/api-simrs-rsudajibarang/api/references/pekerjaan")
    public Call<JsonObject> ListPekerjaan(@Header("Authorization") String auth);

    @GET("/api-simrs-rsudajibarang/api/references/statuspernikahan")
    public Call<JsonObject> ListStatusPernikahan(@Header("Authorization") String auth);

    @GET("/api-simrs-rsudajibarang/api/references/etnis")
    public Call<JsonObject> ListSuku(@Header("Authorization") String auth);

    @GET("/api-simrs-rsudajibarang/api/references/title")
    public Call<JsonObject> ListTitle(@Header("Authorization") String auth);

    @GET("/api-simrs-rsudajibarang/api/references/bahasa")
    public Call<JsonObject> ListBahasa(@Header("Authorization") String auth);

    @GET("/api-simrs-rsudajibarang/api/references/hubunganpasien")
    public Call<JsonObject> ListHubunganPasien(@Header("Authorization") String auth);

    @GET("/api-simrs-rsudajibarang/api/references/carabayar")
    public Call<JsonObject> ListCaraBayar(@Header("Authorization") String auth);

    @GET("/api-simrs-rsudajibarang/api/references/poliklinik")
    public Call<JsonObject> ListPolik(@Header("Authorization") String auth);

    @GET("/api-simrs-rsudajibarang/api/references/poliklinik/{id}")
    public Call<JsonObject> getActivePoliklinik(
            @Path("id") String id,
            @Header("Authorization") String auth);


}
