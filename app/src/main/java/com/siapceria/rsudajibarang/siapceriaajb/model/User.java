package com.siapceria.rsudajibarang.siapceriaajb.model;

import com.google.gson.annotations.SerializedName;

public class User{

	@SerializedName("image")
	private String image;

	@SerializedName("firstname")
	private String firstname;

	@SerializedName("is_active")
	private String isActive;

	@SerializedName("lastip")
	private String lastip;

	@SerializedName("signature")
	private String signature;

	@SerializedName("pegawai_id")
	private String pegawaiId;

	@SerializedName("date_created")
	private String dateCreated;

	@SerializedName("lastlogin")
	private String lastlogin;

	@SerializedName("poli")
	private Object poli;

	@SerializedName("ruang")
	private String ruang;

	@SerializedName("lastname")
	private String lastname;

	@SerializedName("alamat")
	private String alamat;

	@SerializedName("nik")
	private String nik;

	@SerializedName("password")
	private String password;

	@SerializedName("unit")
	private Object unit;

	@SerializedName("role_id")
	private String roleId;

	@SerializedName("online")
	private String online;

	@SerializedName("nohp")
	private String nohp;

	@SerializedName("id")
	private String id;

	@SerializedName("email")
	private String email;

	@SerializedName("tasks")
	private String tasks;

	public void setImage(String image){
		this.image = image;
	}

	public String getImage(){
		return image;
	}

	public void setFirstname(String firstname){
		this.firstname = firstname;
	}

	public String getFirstname(){
		return firstname;
	}

	public void setIsActive(String isActive){
		this.isActive = isActive;
	}

	public String getIsActive(){
		return isActive;
	}

	public void setLastip(String lastip){
		this.lastip = lastip;
	}

	public String getLastip(){
		return lastip;
	}

	public void setSignature(String signature){
		this.signature = signature;
	}

	public String getSignature(){
		return signature;
	}

	public void setPegawaiId(String pegawaiId){
		this.pegawaiId = pegawaiId;
	}

	public String getPegawaiId(){
		return pegawaiId;
	}

	public void setDateCreated(String dateCreated){
		this.dateCreated = dateCreated;
	}

	public String getDateCreated(){
		return dateCreated;
	}

	public void setLastlogin(String lastlogin){
		this.lastlogin = lastlogin;
	}

	public String getLastlogin(){
		return lastlogin;
	}

	public void setPoli(Object poli){
		this.poli = poli;
	}

	public Object getPoli(){
		return poli;
	}

	public void setRuang(String ruang){
		this.ruang = ruang;
	}

	public String getRuang(){
		return ruang;
	}

	public void setLastname(String lastname){
		this.lastname = lastname;
	}

	public String getLastname(){
		return lastname;
	}

	public void setAlamat(String alamat){
		this.alamat = alamat;
	}

	public String getAlamat(){
		return alamat;
	}

	public void setNik(String nik){
		this.nik = nik;
	}

	public String getNik(){
		return nik;
	}

	public void setPassword(String password){
		this.password = password;
	}

	public String getPassword(){
		return password;
	}

	public void setUnit(Object unit){
		this.unit = unit;
	}

	public Object getUnit(){
		return unit;
	}

	public void setRoleId(String roleId){
		this.roleId = roleId;
	}

	public String getRoleId(){
		return roleId;
	}

	public void setOnline(String online){
		this.online = online;
	}

	public String getOnline(){
		return online;
	}

	public void setNohp(String nohp){
		this.nohp = nohp;
	}

	public String getNohp(){
		return nohp;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setEmail(String email){
		this.email = email;
	}

	public String getEmail(){
		return email;
	}

	public void setTasks(String tasks){
		this.tasks = tasks;
	}

	public String getTasks(){
		return tasks;
	}

	@Override
 	public String toString(){
		return 
			"User{" + 
			"image = '" + image + '\'' + 
			",firstname = '" + firstname + '\'' + 
			",is_active = '" + isActive + '\'' + 
			",lastip = '" + lastip + '\'' + 
			",signature = '" + signature + '\'' + 
			",pegawai_id = '" + pegawaiId + '\'' + 
			",date_created = '" + dateCreated + '\'' + 
			",lastlogin = '" + lastlogin + '\'' + 
			",poli = '" + poli + '\'' + 
			",ruang = '" + ruang + '\'' + 
			",lastname = '" + lastname + '\'' + 
			",alamat = '" + alamat + '\'' + 
			",nik = '" + nik + '\'' + 
			",password = '" + password + '\'' + 
			",unit = '" + unit + '\'' + 
			",role_id = '" + roleId + '\'' + 
			",online = '" + online + '\'' + 
			",nohp = '" + nohp + '\'' + 
			",id = '" + id + '\'' + 
			",email = '" + email + '\'' + 
			",tasks = '" + tasks + '\'' + 
			"}";
		}
}