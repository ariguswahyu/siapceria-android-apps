package com.siapceria.rsudajibarang.siapceriaajb.fragment.PelayananPublik;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.siapceria.rsudajibarang.siapceriaajb.R;
import com.siapceria.rsudajibarang.siapceriaajb.constant.Base;
import com.siapceria.rsudajibarang.siapceriaajb.fragment.HomeFragment;
import com.siapceria.rsudajibarang.siapceriaajb.fragment.ProfileRumahSakit.ProfileRSFragment;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link PelayananPublikFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PelayananPublikFragment extends Fragment {

    private String postUrl = Base.WEBSITE_INFORMASI_PUBLIK_RSUDAJIBARANG;
    private WebView webView;
    //    private ImageView back;
    private ProgressBar progressBar;

    public PelayananPublikFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static PelayananPublikFragment newInstance(String param1, String param2) {
        PelayananPublikFragment fragment = new PelayananPublikFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    private void setToolbar(View view) {
        Toolbar toolbar = view.findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        toolbar.setTitle(Base.BACK_TEXT);
        toolbar.setTitleTextColor(getResources().getColor(android.R.color.white));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @SuppressLint("WrongConstant")
            @Override
            public void onClick(View v) {
                HomeFragment secondFragtry = new HomeFragment();
                FragmentManager fm = getActivity().getSupportFragmentManager();
                fm.beginTransaction().replace(R.id.flMain, secondFragtry).commit();
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view =  inflater.inflate(R.layout.fragment_pelayanan_publik, container, false);
        setToolbar(view);
        webView = (WebView) view.findViewById(R.id.webView);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar1);

        webView.setWebViewClient(new PelayananPublikFragment.myWebClient());
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl(postUrl);
        webView.setHorizontalScrollBarEnabled(false);

        return  view;
    }

    public class myWebClient extends WebViewClient {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            // TODO Auto-generated method stub
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            // TODO Auto-generated method stub

            view.loadUrl(url);
            return true;

        }

        @Override
        public void onPageFinished(WebView view, String url) {
            // TODO Auto-generated method stub
            super.onPageFinished(view, url);
            progressBar.setVisibility(View.GONE);
        }
    }
}