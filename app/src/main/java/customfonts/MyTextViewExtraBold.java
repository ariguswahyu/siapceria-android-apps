package customfonts;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by Pluto on 25/12/2017.
 */

public class MyTextViewExtraBold extends androidx.appcompat.widget.AppCompatEditText {

    public MyTextViewExtraBold(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public MyTextViewExtraBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MyTextViewExtraBold(Context context) {
        super(context);
        init();
    }

    private void init() {
        if (!isInEditMode()) {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Nunito-ExtraBold.ttf");
            setTypeface(tf);
        }
    }

}
